<?php require 'cabecalho.php'; ?>

<body style="height: 100%">

<div id="login-page" style="height: 100%">
    <div class="container" style="height: 100%">

        <form class="form-procurar form-login" action="endereco.php" style="height: 100%; width: 100%;">

            <div class="login-wrap" style="height: 100%; padding-top: 50px;">
                <div class="col-md-4 col-xs-1 col-lg-4">
                    &nbsp;
                </div>
                <div class="col-md-4 col-xs-9 col-lg-4">
                    <img src="assets/img/brocadao.png">
                </div>
                <div class="col-md-4 col-xs-2 col-lg-4">
                    &nbsp;
                </div>
                <div class="input-group margin-bottom-sm" style="padding-top: 15px;">

                    <input type="text" id="endereco" name="endereco" class="locc form-control"
                           placeholder="Informe seu Endereço ou seu CEP" autofocus>

                </div>


                <input type="button" class="btn btn-theme btn-block" value="PROCURAR" id="bt-procurar">
                <hr>

                <div class="login-social-link centered">
                    <p>or you can sign in via your social network</p>

                </div>
                <div class="registration">
                    Don't have an account yet?<br/>
                    <a id="ff" class="" href="#">
                        Create an account
                    </a>
                </div>

            </div>

            <!-- Modal -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                 class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Forgot Password ?</h4>
                        </div>
                        <div class="modal-body">
                            <p>Enter your e-mail address below to reset your password.</p>
                            <input type="text" name="email" placeholder="Email" autocomplete="off"
                                   class="form-control placeholder-no-fix">

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            <button class="btn btn-theme" type="button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->

        </form>

    </div>
</div>

<div id="login-page2">
    <div class="container">

        <form class="form-procurar form-login" method="post" action="control/find.php"
              style="margin: 0px ; height: 100%">
            <h2 class="form-login-heading"></h2>
            <div class="login-wrap">

                <div class="col-md-2">


                    <div class="input-group margin-bottom-sm">

                        <input type="text" name="estado" placeholder="Estado" class="form-email form-control"
                               id="estado">
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="input-group margin-bottom-sm">

                        <input type="text" name="cidade" placeholder="Cidade" class="form-email form-control"
                               id="cidade">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group margin-bottom-sm">

                        <input type="text" name="bairro" placeholder="Bairro" class="form-email form-control"
                               id="bairro">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group margin-bottom-sm">

                        <input type="text" name="rua" placeholder="Rua" class="form-email form-control" id="rua">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="input-group margin-bottom-sm">

                        <input type="text" name="complemento" placeholder="Informe o complemento"
                               class="form-email form-control" id="rua">
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="input-group margin-bottom-sm">

                        <input type="text" name="cep" placeholder="cep" class="form-email form-control" id="cep">
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="input-group margin-bottom-sm">
                        <input type="hidden" name="longitude" placeholder="Longitude" class="form-email form-control"
                               id="longitude">
                        <input type="hidden" name="latitude" id="latitude">
                        <input type="hidden" name="radios" id="radius">
                        <button class="btn btn-theme btn-block" href="index.html" type="submit" id="procurar">
                            PROCURAR
                        </button>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group margin-bottom-sm">

                        <input type="button" class="btn btn-theme btn-block" id="goback" value="VOLTAR">
                    </div>
                </div>


                <hr>

                <div class="login-social-link centered">
                    <p>or you can sign in via your social network</p>

                </div>
                <div class="registration">
                    Don't have an account yet?<br/>
                    <a id="bt-procurar" class="" href="#">
                        Create an account
                    </a>
                </div>

            </div>

            <!-- Modal -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                 class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Forgot Password ?</h4>
                        </div>
                        <div class="modal-body">
                            <p>Enter your e-mail address below to reset your password.</p>
                            <input type="text" name="email" placeholder="Email" autocomplete="off"
                                   class="form-control placeholder-no-fix">

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            <button class="btn btn-theme" type="button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->

        </form>
    </div>
</div>

<div id="stack1" class="modal " tabindex="-1" data-width="860">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4>Marque sua localização exata</h4>
        <div class="modal-body" style="padding: 0px">
            <div class="row-fluid">

                <div id="us3" class="teste"></div>

            </div>
            <div class="modal-footer" style="padding: 0px">
                <button type="button" data-dismiss="modal" class="btn">Confirmar</button>
            </div>
        </div>
    </div>
</div>
<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/locationpicker.jquery.min.js"></script>
<script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modal.js"></script>
<!--BACKSTRETCH-->
<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
<script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
<script>
    $.backstretch("assets/img/bg.jpg", {speed: 150});
</script>

<script>


    function updateControls(addressComponents) {
        var pais = addressComponents.country;
        var estado = addressComponents.stateOrProvince;
        var cidade = addressComponents.city;
        var bairro = addressComponents.district;
        var rua = addressComponents.addressLine1;
        var cep = addressComponents.postalCode;

        var ret = rua.split(" ");
        var rua = "";
        var tes = ret.length;


        for (var i = 1; i < tes; i++) {
            rua += ret[i] + " ";
            // more statements
        }

        $('#us3-cidade').val(rua);
        $('#pais').val(pais);
        $('#estado').val(estado);
        $('#cidade').val(cidade);
        $('#bairro').val(bairro);
        $('#rua').val(rua);
        $('#cep').val(cep);

    }

    $("#login-page2").hide();


    $('#us3').locationpicker({
        location: {latitude: 0, longitude: 0},
        radius: 300,
        zoom: 15,
        radius: 1000,
        inputBinding: {
            latitudeInput: $('#latitude'),
            longitudeInput: $('#longitude'),
            radiusInput: $('#radius'),
            locationNameInput: $('#endereco')
        },
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            var addressComponents = $(this).locationpicker('map').location.addressComponents;


            updateControls(addressComponents);
            updateControls(addressComponents);
        }
    });


    function explode() {
        $("#endereco").val("");
    }
    setTimeout(explode, 1000);


    $('#stack1').on('shown.bs.modal', function () {
        $('#us3').locationpicker('autosize');
    });


    $("#bt-procurar").click(function () {
        $("#login-page").hide();
        $("#login-page2").show();


    });

    $("#goback").click(function () {
        $("#login-page2").hide();
        $("#login-page").show();


    });


</script>


</body>
</html>
