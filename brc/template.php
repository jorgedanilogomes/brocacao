<?php require_once 'cabecalho.php'; ?>

<body>

<header class="header black-bg">

    <nav class="navbar navbar-default">


        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="assets/img/minilogo.png"></a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="row ds">
            <div class='col-md-4 col-xs-4'><a href="home.php"><h3 style="color :#fff;">
                        <i class="fa fa-cutlery fa-2x"></i></h3></a>
            </div>

            <div class='col-md-4 col-xs-4'><a href="lista_cardapio.php"><h3><i
                                class="fa fa-pencil-square-o fa-2x"></i></h3></a>
            </div>
            <div class='col-md-4 col-xs-4'><a href="lista_pedido.php"><h3><i class="fa fa-shopping-cart fa-2x"></i>
                    </h3></a>
            </div>

        </div>
    </nav>

</header>

</body>