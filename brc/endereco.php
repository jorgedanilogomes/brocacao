<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>DASHGUM - Bootstrap Admin Template</title>

        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/style-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
        
  <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
        <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal.css" />
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->

      
        
         <div id="login-page2" >
            <div class="container">

                <form class="form-procurar form-login" action="home.html">
                    <h2 class="form-login-heading">Logo</h2>
                    <div class="login-wrap">
                      
                        <div class="input-group margin-bottom-sm">
          <div class="form-group">
                                <label class="sr-only" for="form-email">Pais</label>
                                <input type="text" name="pais" placeholder="Pais" class="form-email form-control" id="pais">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Estado</label>
                                <input type="text" name="estado" placeholder="Estado" class="form-email form-control" id="estado">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Cep</label>
                                <input type="text" name="cep" placeholder="cep" class="form-email form-control" id="cep">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Cidade</label>
                                <input type="text" name="cidade" placeholder="Cidade" class="form-email form-control" id="cidade">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Bairro</label>
                                <input type="text" name="bairro" placeholder="Bairro" class="form-email form-control" id="bairro">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Rua</label>
                                <input type="text" name="rua" placeholder="Rua" class="form-email form-control" id="rua">
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-email">Latitude</label>
                                <input type="text" name="latitude" placeholder="Latitude" class="form-email form-control" id="latitude" >
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Longitude</label>
                                <input type="text" name="longitude" placeholder="Longitude" class="form-email form-control" id="longitude" >
                                <input type="hidden" name="radios"  id="radius" >
                            </div>


                        </div>
                      

                        <button  class="btn btn-theme btn-block" href="index.html" type="submit"> PROCURAR2</button>
                        <hr>

                        <div class="login-social-link centered">
                            <p>or you can sign in via your social network</p>

                        </div>
                        <div class="registration">
                            Don't have an account yet?<br/>
                            <a id="bt-procurar" class="" href="#">
                                Create an account
                            </a>
                        </div>

                    </div>

                    <!-- Modal -->
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Forgot Password ?</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Enter your e-mail address below to reset your password.</p>
                                    <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                    <button class="btn btn-theme" type="button">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal -->

                </form>	  	

            </div>
        </div>
        <div id="stack1" class="modal " tabindex="-1" data-width="860"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Marque sua localização exata</h4>
                <div class="modal-body" style="padding: 0px">
                    <div class="row-fluid">

                        <div id="us3" class="teste"></div>

                    </div>
                    <div class="modal-footer" style="padding: 0px">
                        <button type="button" data-dismiss="modal" class="btn">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/locationpicker.jquery.min.js"></script>
         <script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
            <script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modal.js"></script>
        <!--BACKSTRETCH-->
        <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
        <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
        <script>
            $.backstretch("assets/img/bg.jpg", {speed: 150});
        </script>
      
        <script>
            var latitude = -17.6766109;
            var longitude = -48.275482499999995;
               
    
    
         
                function updateControls(addressComponents) {
                    var pais = addressComponents.country;
                    var estado = addressComponents.stateOrProvince;
                    var cidade = addressComponents.city;
                    var bairro = addressComponents.district;
                    var rua = addressComponents.addressLine1;
                    var cep = addressComponents.postalCode;

                    var ret = rua.split(" ");
                    var rua = "";
                    var tes = ret.length;


                    for (var i = 1; i < tes; i++) {
                        rua += ret[i] + " ";
                        // more statements
                    }

                    $('#us3-cidade').val(rua);
                    $('#pais').val(pais);
                    $('#estado').val(estado);
                    $('#cidade').val(cidade);
                    $('#bairro').val(bairro);
                    $('#rua').val(rua);
                    $('#cep').val(cep);

                }       
                       
      
              
      
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition,showError);
    } else { 

        $('#us3').locationpicker({
                location: {latitude: -16.6766109 , longitude: -49.275482499999995},
                radius: 300,
                zoom: 15,
                radius: 1000,
                        inputBinding: {
                            latitudeInput: $('#latitude'),
                            longitudeInput: $('#longitude'),
                            radiusInput: $('#radius'),
                            locationNameInput: $('#endereco')
                        },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                    var addressComponents = $(this).locationpicker('map').location.addressComponents;


                    updateControls(addressComponents);
                    
                }
            });
            
    }
 


function showPosition(position) {
    this.latitude =  position.coords.latitude; 
    this.longitude = position.coords.longitude;	
      $('#us3').locationpicker({
                location: {latitude: this.latitude, longitude: this.longitude},
                radius: 300,
                zoom: 15,
                radius: 1000,
                        inputBinding: {
                            latitudeInput: $('#latitude'),
                            longitudeInput: $('#longitude'),
                            radiusInput: $('#radius'),
                            locationNameInput: $('#endereco')
                        },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                    var addressComponents = $(this).locationpicker('map').location.addressComponents;


                    updateControls(addressComponents);
                    
                }
            });
           
}
 
 function showError(error)
  {
     $('#us3').locationpicker({
                location: {latitude: -16.6766109 , longitude: -49.275482499999995},
                radius: 300,
                zoom: 15,
                radius: 1000,
                        inputBinding: {
                            latitudeInput: $('#latitude'),
                            longitudeInput: $('#longitude'),
                            radiusInput: $('#radius'),
                            locationNameInput: $('#endereco')
                        },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                    var addressComponents = $(this).locationpicker('map').location.addressComponents;


                    updateControls(addressComponents);
                    
                }
            });
            
          function explode() {
                        $("#endereco").val("");
                    }
                    setTimeout(explode, 1000);
            
  }
 
            
 function updateControls(addressComponents) {
                    var pais = addressComponents.country;
                    var estado = addressComponents.stateOrProvince;
                    var cidade = addressComponents.city;
                    var bairro = addressComponents.district;
                    var rua = addressComponents.addressLine1;
                    var cep = addressComponents.postalCode;

                    var ret = rua.split(" ");
                    var rua = "";
                    var tes = ret.length;


                    for (var i = 1; i < tes; i++) {
                        rua += ret[i] + " ";
                        // more statements
                    }

                    $('#us3-cidade').val(rua);
                    $('#pais').val(pais);
                    $('#estado').val(estado);
                    $('#cidade').val(cidade);
                    $('#bairro').val(bairro);
                    $('#rua').val(rua);
                    $('#cep').val(cep);

                }
            
          
            
           
    $('#stack1').on('shown.bs.modal', function () {
                $('#us3').locationpicker('autosize');
            });
            
            
    $("#bt-procurar").click(function(){
    $("#login-page").hide();
    $("#login-page2").show();
});     
                
        </script>


    </body>
</html>

