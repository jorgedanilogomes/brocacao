<?php require 'template.php'; ?>

<body>

<section id="container" ng-app="brocadao">

    <section id="main-content">

        <section class="wrapper">

            <div class="row ds restaurante_h" ng-controller="fornecedorCtrl">

                <div class="col-lg-6">

                    <div class="row"  ng-init="buscaFornecedorById()">

                        <div class="desc" id="desc" ng-repeat="dado in dados">

                            <!-- DETALHES DO FORNECEDOR ESCOLHIDO -->

                            <div class='col-md-1 col-xs-2'>
                                <div class='thumb'>
                                    <i class='fa fa-cutlery fa-3x'></i>
                                </div>
                            </div>

                            <div class='col-md-9 col-xs-7'>
                                <p><muted>   {{dado.fornecedor.nome}}  </muted><br/>
                                    {{dado.fornecedor.frase}}
                                </p>
                            </div>

                            <div class='col-md-2 col-xs-3' >
                                <div class='pull-right' ng-repeat="formaspagamento in dado.pagamento" >
                                    <img ng-src='assets/img/{{formaspagamento.img}}'>&nbsp;
                                </div>
                            </div>

                            <div class='foot'>
                                <div class='pull-left'> </div>
                                <div class='pull-right'><span>Valor Minimo R$  {{dado.fornecedor.valor_minimo}}  - <i class='fa fa-map-marker'></i> {{dado.fornecedor.rua}}   -  {{dado.fornecedor.estado}}   </span></div>
                            </div>

                            <!-- FIM DETALHES DO FORNECEDOR ESCOLHIDO -->

                        </div>

                    </div>

                </div>

            </div>

            <!-- LISTA DE CARDAPIO  -->

            <div class="row ds" id="cardapio" ng-controller="cardapioCtrl" ng-init="buscaCardapio()" >

                <div ng-repeat="dado in cardapio">

                    <a href='./add_item.php?id_cardapio={{dado.id_cardapio}}'>
                        <div class='col-lg-6'>
                            <div class='row'>
                                <div class='desc'>
                                    <div class='lv col-md-1 col-xs-1'>
                                        <div class='thumb' >
                                            <img class='mg-circle' ng-src="assets/img/{{dado.img}}" width='45px' height='45px' align=''>
                                        </div>
                                    </div>
                                    <div class=' col-md-9 col-xs-8'>

                                        <div class=' details'>

                                            <p><muted>  {{dado.nome}}  </muted><br/>
                                                {{dado.descricao}}
                                            </p>

                                        </div>

                                    </div>
                                    <div class='lv col-md-2 col-xs-3' >

                                        <div class='pull-right'><span class='plus glyphicon glyphicon-plus'></span></div>
                                    </div>
                                    <div class='foot'>
                                        <div class='pull-left'></div>
                                        <div class='pull-right'><span ><b>R$  {{dado.valor}}  </b></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                </div>

            </div>

            <!-- FIM LISTA DE CARDAPIO  -->


        </section>

    </section>
    <div class="popout">
        <div class="btn">
            <i class="icon ion-ios-glasses"></i>
        </div>
    </div>


    <!-- FOOTER -->

    <footer class="footer2">
        <div id="float" style="z-index: 2;">

            <a href="lista_pedido.php"> <i class="fa fa-shopping-cart fa-2x"
                                           style="padding-left: 6px; padding-top: 6px;"></i><span
                        style="color : #fff"></span></a>
        </div>
        <div class="col-md-6 col-xs-6">
            <div class="text-left" style="margin-top: 5px;">
                Taxa de entrega
                <div ng-controller="fornecedorCtrl">
                    <div ng-init="buscaFornecedorById()">
                        <b id="te" >R$ {{valorEntrega}}</b>
                    </div>
                </div>



                <br>

                <!-- LISTA TOTAL PEDIDO  -->

                <div ng-controller="pedidosCtrl" ng-init="calculaTotalPedido()">
                    <b id="toi">{{tot_item}}</b> Itens R$ <b id="tov">{{tov}}</b>
                </div>

                <!-- FINAL LISTA TOTAL PEDIDO  -->


            </div>
        </div>
        <div class="col-md-6 col-xs-6">
            <div class="text-right" style="margin-right: 50px; margin-top: 5px; color: #8DEA55;">
                <a href="lista_pedido.php" style="color: #8DEA55;"> Clique aqui para continuar >></a>

            </div>
        </div>
    </footer>

    <!-- FIM FOOTER -->

</section>

</body>
</html>
