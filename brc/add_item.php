<?php require 'template.php'; ?>

<body>

<section id="container">

    <section id="main-content">
        <section class="wrapper">
            <form method="post" action="control/add_item.php">

                <div class="row restaurante_h">


                    <div class="col-lg-6">
                        <div class="row">

                            <div class="col-md-6 col-xs-12" id="for" style="padding-top: 25px">

                            </div>


                        </div>
                    </div>
                </div>


                <div class="row ds">
                    <div class="col-lg-4 col-md-4 col-sm-6 mb">

                        <div class="white-panel pn">
                            <div class="white-header ">
                                <h5>Item selecionado</h5>
                            </div>

                            <div class="row lstpd">

                                <!-- DIV CONTENDO O CARDÁPIO -->

                                <div class="row lstpd" id="cardapio">

                                    <div class="col-md-1 col-xs-2">
                                        <img class="img-circle" src="assets/img/pizza.png" width="45px" height="45px"
                                             align="">
                                    </div>
                                    <div class="col-md-7 col-xs-6">
                                        <div class="detailsd details">
                                            <p>
                                                <muted>X-BACON</muted>
                                                <br/>
                                                Pão, hamburger, ovo, batata
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        <div class="input-group">
                        <span class="input-group-btn">
                        <button type="button" class="pls btn btn-danger btn-number"
                        <span class="glyphicon glyphicon-minus"></span>
                            </button>
                        </span>
                                            <input type="text" class="plsqtd form-control input-number" value="1"
                                                   min="1" max="99">
                                            <span class="input-group-btn mns">
                        <button type="button" class="pls btn btn-success btn-number">
                        <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        </span>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="pull-right"><span class="total"><b>TOTAL R$ <spam id="tot">  12.00  </span></b></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="white-header ">
                                    <h5>Escolha seus Adicionais</h5>
                                </div>
                                <div class="col-md-12">

                                    <div class="row" id="complementos">


                                    </div>


                                </div>
                                <div id="btnf" class="col-md-12" style="padding: 0px;">
                                    <input type="submit" class="btn btn-theme2 pull-right bt-pedir"
                                           value="Adicionar ao pedido">
                                </div>
                            </div>




                        </div>

                    </div><!-- /row ds -->

        </section>
    </section>

</section>

<div class="footer">

    <div class="col-xs-12 fot">
        <input type="submit" class="btn btn-theme2 pull-right bt-pedir" value="Adicionar ao pedido">

    </div>

</div>


<script>
    $(document).ready(function () {
        $.ajax({
            url: "../admin/index_api.php?controle=Fornecedor&acao=getforbyid&latitude=<?php echo base64_decode($_COOKIE['latitude']) ?>&longitude=<?php echo base64_decode($_COOKIE['longitude']) ?>&id=<?php echo base64_decode($_COOKIE['fornecedor']) ?>",
            //force to handle it as text
            dataType: "json",
            success: function (data) {
                $.each(data, function (i, value) {
                    var conteudo = "<i class='fa fa-cutlery '></i> <muted>" + value.fornecedor.nome + "</muted> <div class='pull-right'><span><i class='fa fa-map-marker'></i> rua " + value.fornecedor.rua + " - " + value.fornecedor.estado + " </span></div>";


                    $("#for").append(conteudo);
                });
            }
        });


        $.ajax({
            url: "../admin/index_api.php?controle=Fornecedor&acao=buscaCardapioById&id_fornecedor=<?php echo base64_decode($_COOKIE['fornecedor']) ?>&id_cardapio=<?php echo $_GET['id_cardapio'] ?>",
            //force to handle it as text
            dataType: "json",
            success: function (data) {
                console.log(data);
                $.each(data, function (i, value) {


                    var conteudo = "<input type='hidden' name='cardapio' value='" + value.cardapio.id_cardapio + "'>" +
                        "<input type='hidden' name='valor_cardapio' value='" + value.cardapio.valor + "'>" +
                        "<input type='hidden' name='nome_cardapio' value='" + value.cardapio.nome + "'>" +
                        "<div class='col-md-1 col-xs-2'>" +
                        "<img class='img-circle' src='assets/img/" + value.cardapio.img + "' width='45px' height='45px' align=''>" +
                        "</div>" +
                        "<div class='col-md-7 col-xs-6'>" +
                        "<div class='detailsd details'>" +
                        "<p><muted>" + value.cardapio.nome + "</muted><br/>" +
                        value.cardapio.descricao +
                        "</p>" +
                        "</div>" +
                        "</div>" +
                        "<div class='col-md-4 col-xs-4'>" +
                        "<div class='input-group'>" +
                        "<span class='input-group-btn'>" +
                        "<button type='button' class='pls btn btn-danger btn-number' onclick='subtrait(" + value.cardapio.id_cardapio + "," + value.cardapio.valor + ")'  data-type='minus' data-field='quantt[" + value.cardapio.id_cardapio + "]'>" +
                        "<span class='glyphicon glyphicon-minus'></span>" +
                        "</button>" +
                        "</span>" +
                        "<input type='text' name='quantt[" + value.cardapio.id_cardapio + "]' class='plsqtd form-control input-number' value='1' min='1' max='99'>" +
                        "<span class='input-group-btn mns'>" +
                        "<button type='button' class='pls btn btn-success btn-number' onclick='somat(" + value.cardapio.id_cardapio + ")'  data-type='plus' data-field='quantt[" + value.cardapio.id_cardapio + "]'>" +
                        "<span class='glyphicon glyphicon-plus'></span>" +
                        "</button>" +
                        "</span>" +
                        "</div>" +
                        "</div>" +
                        "<div  class='col-md-12 col-xs-12'>" +
                        "<div class='pull-right'><span class='total'><b>TOTAL R$ <spam id='tot'>" + value.cardapio.valor + "</span></b></span></div>" +
                        "</div>";

                    $("#cardapio").append(conteudo);


                    $.each(value.complemento, function (i, value2) {
                        var conteudo = "<div class='pd-list desc'>" +

                            "<div class='col-md-6 col-xs-5'>" +
                            "+ <span class='badge bg-important'>" + value2.descricao + "</span>" +
                            "</div>" +
                            "<div class='col-md-3 col-xs-4'>" +
                            "<input type='hidden' name='valor_complemento[" + value2.id_complemento + "]' value='" + value2.valor + "'>" +
                            "<input type='hidden' name='descricao_complemento[" + value2.id_complemento + "]' value='" + value2.descricao + "'>" +
                            "<div class='input-group'>" +
                            "<span class='input-group-btn'>" +
                            "<button type='button' class='pls btn btn-danger btn-number' onclick='subtrai(" + value2.valor + "," + value2.id_complemento + "," + value.cardapio.id_cardapio + ")'  data-type='minus' data-field='quant[" + value2.id_complemento + "]'>" +
                            "<span class='glyphicon glyphicon-minus'></span>" +
                            "</button>" +
                            "</span>" +
                            "<input type='text' name='quant[" + value2.id_complemento + "]' class='plsqtd form-control input-number' value='0' min='0' max='100'>" +
                            "<span class='input-group-btn mns'>" +
                            "<button type='button' class='pls btn btn-success btn-number' onclick='soma(" + value2.valor + "," + value.cardapio.id_cardapio + ")'  data-type='plus' data-field='quant[" + value2.id_complemento + "]'>" +
                            "<span class='glyphicon glyphicon-plus'></span>" +
                            "</button>" +
                            "</span>" +
                            "</div>" +
                            "</div>" +
                            "<div class='col-md-2 col-xs-3'>" +
                            "R$ " + value2.valor +
                            "</div>";


                        $("#complementos").append(conteudo);
                    });
                });
                $('.btn-number').click(function (e) {
                    e.preventDefault();

                    fieldName = $(this).attr('data-field');
                    type = $(this).attr('data-type');
                    var input = $("input[name='" + fieldName + "']");
                    var currentVal = parseInt(input.val());
                    if (!isNaN(currentVal)) {
                        if (type == 'minus') {

                            if (currentVal > input.attr('min')) {
                                input.val(currentVal - 1).change();
                            }
                            if (parseInt(input.val()) == input.attr('min')) {
                                $(this).attr('disabled', true);
                            }

                        } else if (type == 'plus') {

                            if (currentVal < input.attr('max')) {
                                input.val(currentVal + 1).change();
                            }
                            if (parseInt(input.val()) == input.attr('max')) {
                                $(this).attr('disabled', true);
                            }

                        }
                    } else {
                        input.val(0);
                    }
                });

                $('.input-number').focusin(function () {
                    $(this).data('oldValue', $(this).val());
                });
                $('.input-number').change(function () {

                    minValue = parseInt($(this).attr('min'));
                    maxValue = parseInt($(this).attr('max'));
                    valueCurrent = parseInt($(this).val());

                    name = $(this).attr('name');
                    if (valueCurrent >= minValue) {
                        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
                    } else {
                        alert('Sorry, the minimum value was reached');
                        $(this).val($(this).data('oldValue'));
                    }
                    if (valueCurrent <= maxValue) {
                        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
                    } else {
                        alert('Sorry, the maximum value was reached');
                        $(this).val($(this).data('oldValue'));
                    }


                });
                $(".input-number").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                        // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });


            }
        });


    });


    function soma(valor, id) {
        var inputt = $("input[name='quantt[" + id + "]'");
        var nt = parseFloat($("#tot").html().trim()) + (parseFloat(valor) * parseFloat(inputt.val()));
        $("#tot").html(nt.toFixed(2));
    }

    function subtrai(id, input, id2) {

        var inputt = $("input[name='quant[" + input + "]'");
        var input = $("input[name='quantt[" + id2 + "]'");

        if (parseInt(inputt.val()) == 0) {

        }
        else {
            var nt = parseFloat($("#tot").html().trim()) - (parseFloat(id) * parseFloat(input.val()));
            $("#tot").html(nt.toFixed(2));
        }
    }


    function somat(id) {
        var inputt = $("input[name='quantt[" + id + "]'");


        var nv = parseFloat($("#tot").html().trim()) / ((parseFloat(inputt.val())));
        var nt = parseFloat($("#tot").html().trim()) + parseFloat(nv);

        $("#tot").html(nt.toFixed(2));
    }

    function subtrait(input, valor) {

        var inputt = $("input[name='quantt[" + input + "]'");

        if (parseInt(inputt.val()) == 1) {

        }
        else {


            var nv = parseFloat($("#tot").html().trim()) / ((parseFloat(inputt.val())));
            var nt = parseFloat($("#tot").html().trim()) - parseFloat(nv);

            $("#tot").html(nt.toFixed(2));
        }
    }


</script>
</form>
<script>

</script>
</body>
</html>
