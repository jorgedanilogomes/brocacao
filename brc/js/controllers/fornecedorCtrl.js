
app.controller('fornecedorCtrl', function( $scope, $http ) {

    /**
     * Função responsável por iniciar os dados.
     */
    $scope.iniciarDados = function() {
        $scope.buscaFornecedorByDistance();
    }

    /**
     * Responsável por buscar os fornecedores passando as coordenadas do usuário.
     */
    $scope.buscaFornecedorByDistance = function() {

        var latitude = window.atob(getCookie('latitude'));
        var longitude = window.atob(getCookie('longitude'));

        $http({
            method: 'POST',
            url: '../admin/index_api.php?controle=Fornecedor&acao=buscaFornecedorByDistance',
            data : {
                latitude : latitude,
                longitude : longitude
            }
        }).then(function success(response) {
            $scope.dados = response.data;

        });

    };


    /**
     * Responsável por selecionar o fornecedor e direcionar para a lista de cardapio.
     * @param id_fornecedor
     */
    $scope.selecionaFornecedor = function( id_fornecedor ) {
        var date = new Date().getDay();
        console.log(id_fornecedor);

        setCookie('fornecedor', window.btoa(id_fornecedor), date );
        window.location.href = "../brc/lista_cardapio.php";
    };


    /**
     * Função responsável por buscar o fornecedor.
     */
    $scope.buscaFornecedorById = function( ) {

        var latitude = window.atob(getCookie('latitude'));
        var longitude = window.atob(getCookie('longitude'));

        $http({
            method : 'POST',
            url : '../admin/index_api.php?controle=Fornecedor&acao=getforbyid',
            data : {
                latitude: latitude,
                longitude: longitude
            }
        }).then(function success(response) {
            $scope.valorEntrega = response.data[0].fornecedor.valor_frete;
        })
    };


    /**
     * Função responsável por buscar um determinado cookie.
     * @param nomecookie
     * @returns nome cookie setado.
     */
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }


    /**
     * Funcção responsável por setar os cookies.
     * @param nomecookie
     * @param valorcookie
     * @param diaexpiracao
     */
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }


});