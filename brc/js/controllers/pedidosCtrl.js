
app.controller('pedidosCtrl', function( $scope, $http ) {

    /**
     * Responsável calcular o total de pedidos.
     */
    $scope.calculaTotalPedido = function() {

        var totv_item = 0;
        var tot_complemento = 0;
        var totv_complemento = 0;
        $scope.tot_item = 0;

        getCookie()

        $http({
           method: 'GET',
           url: '../admin/index_api.php?controle=Fornecedor&acao=listtotped',
        }).then(function success(response) {
            angular.forEach(response.data, function(value) {
                totv_item += parseFloat(value.item.valor_cardapio) * parseFloat(value.item.qtd_item);
                $scope.tot_item += parseFloat(value.item.qtd_item);

                angular.forEach(value.complementos, function(value2) {
                    totv_complemento += parseFloat(value2.valor_unitario) * parseFloat(value.item.qtd_item);
                    tot_complemento++;
                });
            });

            $scope.tov = totv_item + totv_complemento;

        });
    };


    /**
     * Função responsável por buscar um determinado cookie.
     * @param nomecookie
     * @returns nome cookie setado.
     */
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});