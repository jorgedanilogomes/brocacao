<?php require 'template.php'; ?>

<section id="container" ng-app="brocadao" ng-controller="fornecedorCtrl">

    <section id="main-content">

        <section class="wrapper">

            <div class="row ds" id="conteudo" ng-init="iniciarDados()">

                <div class="col-lg-6" ng-repeat="dado in dados">

                    <div class="row" ng-click="selecionaFornecedor(dado.fornecedor.id_fornecedor)">

                        <div class="desc">

                            <div class="col-md-1 col-xs-2">
                                <div class="thumb">
                                    <img class="img-circle" ng-src="assets/img/{{dado.fornecedor.img}}" width="45px" height="45px" align="">
                                </div>
                            </div>

                            <div class="col-md-11 col-xs-10">

                                <div class="pull-right" ng-repeat="formaspagamento in dado.pagamento">
                                    <img ng-src="assets/img/{{formaspagamento.img}}">&nbsp;"
                                </div>

                                <div class="details">
                                    <p><muted><b>{{dado.fornecedor.nome}}</b></muted><br/>
                                        {{dado.fornecedor.frase}}
                                    </p>
                                </div>

                            </div>

                            <div class="foot">
                                <div class="pull-left"></div>
                                <div class="pull-right"><span><i class="fa fa-map-marker"></i> {{dado.fornecedor.rua}} {{dado.fornecedor.cidade}} - {{dado.fornecedor.estado}} </span></div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

    </section>

</section>

</body>

</html>
