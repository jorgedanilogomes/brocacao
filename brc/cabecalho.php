<!DOCTYPE html>

<html lang="pt-br" style="height: 100%">
    
    <head>
      
        <meta charset="utf-8">
    
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <meta name="description" content="">
      
        <meta name="author" content="Dashboard">
      
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>Brocadão</title>

        <link href="assets/css/bootstrap.css" rel="stylesheet">

        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

        <link href="assets/css/style.css" rel="stylesheet">
       
        <link href="assets/css/style-responsive.css" rel="stylesheet">
       
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyD8ycY-1IkXkwWmPmUcYVPx-r2eow-pkyA&libraries=places'></script>

        <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
       
        <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal.css" />

        <script src="assets/js/jquery.js"></script>

        <script src="assets/js/jquery-1.8.3.min.js"></script>

        <script src="assets/js/bootstrap.min.js"></script>

        <script src="js/angular.js"></script>

        <script src="js/app/app.js"></script>

        <script src="js/controllers/fornecedorCtrl.js"></script>

        <script src="js/controllers/cardapioCtrl.js"></script>

        <script src="js/controllers/pedidosCtrl.js"></script>


    </head>

