<?php require_once 'template.php'; ?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">


        <div class="row ds restaurante_h">
            <div class="col-lg-6">
                <div class="row">

                    <div class="col-md-6 col-xs-12" id="for">

                    </div>


                </div>
            </div>


        </div>
        <div class="row ds">
            <div class="col-lg-4 col-md-4 col-sm-6 mb">
                <!-- WHITE PANEL - TOP USER -->
                <div class="white-panel pn">
                    <div class="white-header ">
                        <h5>Meu Pedido</h5>
                    </div>


                    <div class="row lstpd" id="listap">


                    </div>


                </div>
            </div>

        </div><!-- /row ds -->
        <div id="stack1" class="modal " tabindex="-1" data-width="860">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Marque sua localização exata</h4>
                <div class="modal-body" style="padding: 0px">
                    <div class="row-fluid">

                        <div id="us3" class="teste"></div>

                    </div>
                    <div class="modal-footer" style="padding: 0px">
                        <button type="button" data-dismiss="modal" class="btn">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>


<footer class="footer2">

    <div class="col-md-6 col-xs-6">
        <div class="text-left" style="margin-top: 5px;">

            Taxa de entrega <b id="te">R$ 0,0</b>
            <br>
            <div>
                <b id="toi">0</b> Itens R$ <b id="tov">0,0</b>
            </div>

        </div>
    </div>
    <div class="col-md-6 col-xs-6">
        <button data-toggle="modal" data-target="#stack1" href="#stack1" id="processar"
                class="pull-right btn btn-theme-finish"><span>Finalizar pedido</span> <span><i
                        class="fa fa-shopping-cart fa-2x"></i></span></button>
    </div>

</footer>


</section>


<script>

    $(document).ready(function () {
        $("#flip").click(function () {
            $("#panel").slideToggle("slow");
        });


        requestpd();


    });

    function requestpd() {
        $("#listap").html("");
        $.ajax({
            url: "./control/fornecedor.php?action=getforbyid&latitude=<?php echo base64_decode($_COOKIE['latitude']) ?>&longitude=<?php echo base64_decode($_COOKIE['longitude']) ?>&id=<?php echo base64_decode($_COOKIE['fornecedor']) ?>",
            //force to handle it as text
            dataType: "json",
            success: function (data) {
                $.each(data, function (i, value) {
                    var conteudo = "<i class='fa fa-cutlery '></i> <muted>" + value.fornecedor.nome + "</muted> <div class='pull-right'><span><i class='fa fa-map-marker'></i> rua " + value.fornecedor.rua + " - " + value.fornecedor.estado + " </span></div>";


                    $("#for").append(conteudo);
                });
            }
        });


        $.ajax({
            url: "./control/fornecedor.php?action=listtotped",
            //force to handle it as text
            dataType: "json",
            success: function (data) {
                var totv_item = 0;
                var tot_item = 0;
                var tot_complemento = 0;
                var totv_complemento = 0;
                var conteudo = "";
                $.each(data, function (i, value) {
                    var totrm = 0;
                    conteudo += "<input type='hidden' name='cardapio[" + value.item.id_cadapio + "]' value='" + value.item.id_cadapio + "'>" +
                        "<div class='pd-list desc'>" +

                        "<div class='dscp col-md-6 col-xs-6'> <b>" +
                        value.item.nome_cardapio +
                        "</div></b>" +

                        "<div class='dscp col-md-6 col-xs-6 nopadding' style='float: right;' >" +
                        "<div class='pull-right'>" +
                        "<div class='input-group' style='margin-right: 0px;'>" +
                        "<span class='input-group-btn'>" +
                        "<button type='button' class='pls btn btn-danger btn-number' onclick='subtrai(" + i + ")'  data-type='minus' data-field='quant[" + i + "]'>" +
                        "<span class='glyphicon glyphicon-minus'></span>" +
                        "</button>" +
                        "</span>" +
                        "<input type='text' name='quant[" + i + "]' class='plsqtd form-control input-number' value='" + value.item.qtd_item + "' min='1' max='100'>" +
                        "<span class='input-group-btn mns'>" +
                        "<button type='button' class='pls btn btn-success btn-number' onclick='soma(" + i + ")' data-type='plus' data-field='quant[" + i + "]'>" +
                        "<span class='glyphicon glyphicon-plus'></span>" +
                        "</button>" +
                        "</span>" +
                        "</div>" +
                        "</div>" +
                        "</div>";


                    // alert(value.item.valor_cardapio);
                    totv_item += parseFloat(value.item.valor_cardapio) * parseFloat(value.item.qtd_item);
                    tot_item = tot_item + parseFloat(value.item.qtd_item);
                    var tcomp = 0;
                    $.each(value.complementos, function (i, value2) {

                        conteudo += "<div class='col-md-6 col-xs-6' style='height: 35px;'>" +
                            "+ <span class='badge bg-important' style='margin-left: 15px'>" + value2.qtd + " " + value2.descricao + "</span>" +


                            "</div>" +

                            "<div class='col-md-6 col-xs-6' style='height: 35px;'><div class='pull-right'>" +

                            "</div></div>";

                        totv_complemento += parseFloat(value2.valor_unitario) * parseFloat(value.item.qtd_item);
                        tcomp += parseFloat(value2.valor_unitario);
                        tot_complemento++;
                    });
                    totrm = (parseFloat(value.item.valor_cardapio) + parseFloat(tcomp)) * parseFloat(value.item.qtd_item);

                    conteudo += "<input type='hidden' name='totrm[" + i + "]' value='" + totrm + "'>";
                    conteudo += "<div class='col-md-6 col-xs-6' style='margin-top: 10px;'><a href='./control/del_item.php?item=" + i + "'>   <i class='fa fa-trash-o'></i></a></div>" +
                        "<div class='col-md-6 col-xs-6' style='margin-top: 10px;'><div class='pull-right '><b>R$ " + totrm.toFixed(2) + "</b></div></div></div>";


                });
                $("#listap").append(conteudo);
                var tov = totv_item + totv_complemento;
                $("#toi").html(tot_item);
                $("#tov").html(tov.toFixed(2));

                $('.btn-number').click(function (e) {
                    e.preventDefault();

                    fieldName = $(this).attr('data-field');
                    type = $(this).attr('data-type');
                    var input = $("input[name='" + fieldName + "']");
                    var currentVal = parseInt(input.val());
                    if (!isNaN(currentVal)) {
                        if (type == 'minus') {

                            if (currentVal > input.attr('min')) {
                                input.val(currentVal - 1).change();
                            }
                            if (parseInt(input.val()) == input.attr('min')) {
                                $(this).attr('disabled', true);
                            }

                        } else if (type == 'plus') {

                            if (currentVal < input.attr('max')) {
                                input.val(currentVal + 1).change();
                            }
                            if (parseInt(input.val()) == input.attr('max')) {
                                $(this).attr('disabled', true);
                            }

                        }
                    } else {
                        input.val(0);
                    }
                });

                $('.input-number').focusin(function () {
                    $(this).data('oldValue', $(this).val());
                });
                $('.input-number').change(function () {

                    minValue = parseInt($(this).attr('min'));
                    maxValue = parseInt($(this).attr('max'));
                    valueCurrent = parseInt($(this).val());

                    name = $(this).attr('name');
                    if (valueCurrent >= minValue) {
                        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
                    } else {
                        alert('Sorry, the minimum value was reached');
                        $(this).val($(this).data('oldValue'));
                    }
                    if (valueCurrent <= maxValue) {
                        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
                    } else {
                        alert('Sorry, the maximum value was reached');
                        $(this).val($(this).data('oldValue'));
                    }


                });
                $(".input-number").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                        // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });

            }

        });
    }
    function soma(id) {
        var inputt = $("input[name='totrm[" + id + "]'");

        var nt = parseFloat($("#tov").html().trim()) + parseFloat(inputt.val());
        $("#tov").html(nt.toFixed(2));
    }

    function subtrai(id) {
        var input = $("input[name='quant[" + id + "]'");
        var inputt = $("input[name='totrm[" + id + "]'");
        var nt = 0;

        if (parseInt(input.val()) == 1) {

        }
        else {

            nt = parseFloat($("#tov").html().trim()) - parseFloat(inputt.val());
            $("#tov").html(nt.toFixed(2));
        }
    }
</script>
<script>
    $("#processar").click(function () {
        $('#stack1').on('shown.bs.modal', function () {

        });
    });

</script>
</body>
</html>
