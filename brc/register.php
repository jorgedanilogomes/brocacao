<!DOCTYPE html>
<html lang="en" style="height: 100%">
<?php
include_once 'template/header.php';

?>

  <body style="height: 100%">

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page" style="height: 100%">
	  	<div class="container" style="height: 100%">
	  	
                    <form class="form-login2 form-login" method="post" id="register" name="register" action="control/cliente.php?action=store">
		        <h2 class="form-login-heading">Registre-se</h2>
		        <div class="login-wrap" style="height: 100%;padding-top: 50px;">
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome Completo" autofocus>
                            <br>
		            <input type="text" id="email" name="email" class="form-control" placeholder="Email" autofocus>
		          
                            <br>
		            <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha">
                            <br>
                            <input type="password" id="senha2" name="senha2" class="form-control" placeholder="Repita a senha">
                            <br>
                            <input class="btn btn-theme btn-block" id="registrar" type="submit" value="REGISTRAR"  >
		            <hr>
		            
		           
		
		        </div>
		
		        
		
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    
 <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
        <link rel="stylesheet" href="http://jqueryvalidation.org/files/demo/site-demos.css">
    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script>
  $(function () {
      
      
      
    $.validator.addMethod(
                    "uniqueUserName",
                    function (value, element) {
                        $.ajax({
                            type: "POST",
                            url: "control/fornecedor.php?action=exist",
                            data: "email=" + value,
                            dataType: "html",
                            success: function (msg)
                            {
                                var json = $.parseJSON(msg);
                                //If username exists, set response to true
                                if (json.mail == 'true') {
                                    response = false;
                                }
                                else {
                                    response = true;
                                }

                            }
                        });
                        return response;
                    },
                    "Já existe um usuário com este email!"
                );    
  
  $("#register").validate({
                        rules: {
                            senha: {
                                required: true,
                                minlength: 5
                            },
                            email: {
                                required: true,
                                email: true,
                                uniqueUserName: true
                            }
                            ,
                            senha2: {
                                required: true,
                                minlength: 5,
                                equalTo: "#senha"
                            },
                            complemento: {
                                required: true

                            },
                            nome: {
                                required: true

                            }

                        },
                        messages: {
                            senha: {
                                required: "Informe a senha",
                                minlength: "Sua senha deve possuir no minimo 5 caracteres"
                            },
                            senha2: {
                                required: "Informe a senha",
                                minlength: "Sua senha deve possuir no minimo 5 caracteres",
                                equalTo: "As senhas não estão iguais!"
                            },
                            nome: {
                                required: "Este campo é obrigatório"
                            },
                            email: {
                                required: "Este campo é obrigatório"
                            }
                        }
                    });
                    });
</script>

  </body>
</html>
