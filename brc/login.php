<!DOCTYPE html>
<html lang="en" style="height: 100%">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="height: 100%">

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page" style="height: 100%">
	  	<div class="container" style="height: 100%">
	  	
                    <form class="form-login2 form-login" method="post" action="control/login.php">
		        <h2 class="form-login-heading">Faça seu login</h2>
		        <div class="login-wrap" style="height: 100%;padding-top: 50px;">
		            <input type="text" name="email" class="form-control" placeholder="User ID" autofocus>
		            <br>
		            <input type="password" name="senha" class="form-control" placeholder="Password">
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal" href="login.html#myModal" style="color: #fff;"> Esqueceu a senha?</a>
		
		                </span>
		            </label>
		            <button class="btn btn-theme btn-block"  type="submit"><i class="fa fa-lock"></i> ENTRAR </button>
		            <hr>
		            
		            <div class="login-social-link centered">
		            <p style="color: #fff;">ou você também pode entrar por sua rede social</p>
		                <button class="btn btn-facebook" type="submit" style="width: 100%;height: 50px"><i class="fa fa-facebook"></i> Facebook</button>
		                
		            </div>
		            <div class="registration" style="color: #fff;">
		                Ainda não tem uma conta no brocadão?<br/>
		                <a class="" href="#" >
		                    Criar nova conta
		                </a>
		            </div>
		
		        </div>
		
		          <!-- Modal -->
		          
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
