<?php

     
require_once '../dao/FornecedorDb.php';
$db = new FornecedorModel();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['email']) && isset($_POST['senha'])) {
 
    // receiving the post params
    $email = $_POST['email'];
    $password = $_POST['senha'];
 
    // get the user by email and password
    $user = $db->getUserByEmailAndPassword($email, $password);
 
    if ($user != false) {
        // use is found
       $usertoken = $db->storeToken($email) ;
 
      setcookie("token", $usertoken['token'],time()+(60*60*24*365),"/");
      setcookie("email", $usertoken['email'],time()+(60*60*24*365),"/");
      setcookie("id_fornecedor", $user['id_fornecedor'],time()+(60*60*24*365),"/");
    // echo $_COOKIE["token2"];     
 header ("Location: ../home/pages/");
     
    } else {
        // user is not found with the credentials
        header("location: ../index.php?message=fail"); 
    
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email or password is missing!";
    echo json_encode($response);
}
?>