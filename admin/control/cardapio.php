<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

@$action = $_REQUEST['action'];

if ($action == "categoria") {
    require_once '../dao/ComplementoDB.php';
    $complementoDB = new ComplementoDB();
    $categoria = $complementoDB->getAllCategoria();
    echo json_encode($categoria);
    
}

elseif ($action == "store") {
    @$id_fornecedor = $_COOKIE['id_fornecedor'];
    $categoria = @$_POST['categoria'];
    $valor = @str_replace(",", ".", str_replace(".", "", $_POST['valor']));
    $descricao = @$_POST['descricao'];
    $nome = @$_POST['nome'];
    $tempo = @$_POST['horas'] + @$_POST['minutos'];
    $id_cardapio = @$_POST['id_cardapio'];

    if(empty($id_cardapio)){
    if (empty($descricao) or empty($valor) or empty($categoria) or empty($nome)) {
        echo "{\"erro\": \"Existem campos não informados\"}";
    } else {
        //    return false;



     
        require_once '../dao/CardapioDB.php';
        $cardapio = new CardapioDB();
        $cardapio = $cardapio->storeCardapio($nome, $id_fornecedor, $descricao, $categoria, $valor, $tempo);
        if ($cardapio) {
            echo "{\"erro\": \"false\"}";
        } else {
            //  return false;
            echo $cardapio;
        }
    }
    }
    else{
         require_once '../dao/CardapioDB.php';
        $cardapio = new CardapioDB();
        $cardapio = $cardapio->updateCardapio($nome, $tempo, $descricao, $valor, $categoria, $id_fornecedor, $id_cardapio);
        if ($cardapio) {
            echo "{\"erro\": \"false\"}";
        } else {
            //  return false;
            echo $cardapio;
        }
        
    }
}

elseif ($action=="getall") {
      @$id_fornecedor = $_COOKIE['id_fornecedor'];
 require_once '../dao/CardapioDB.php';
    $cardapioDB = new CardapioDB();
    $cardapio = $cardapioDB->getAllCardapio($id_fornecedor);
    
    echo json_encode($cardapio);
    
}
elseif ($action=="get") {
      @$id_fornecedor = $_COOKIE['id_fornecedor'];
 require_once '../dao/CardapioDB.php';
    @$id_cardapio = $_REQUEST['id_cardapio'];
    $cardapioDB = new CardapioDB();
    $cardapio = $cardapioDB->getCardapio($id_cardapio,$id_fornecedor);
    echo json_encode($cardapio);
    
}
elseif ($action=="delete") {
    require_once '../dao/CardapioDB.php';
     @$id_fornecedor = $_COOKIE['id_fornecedor'];
     $cardapio = new CardapioDB();
     @$id_cardapio = $_REQUEST['id_cardapio'];
   $cardapio->deleteCardapio($id_cardapio, $id_fornecedor);
    
    echo "{\"erro\": \"delete\"}";
    

}

