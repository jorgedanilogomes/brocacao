<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$action = $_REQUEST['action'];

if($action=="store"){

require_once '../dao/FornecedorDb.php';    

$fornecedorDb = new FornecedorModel();
    
if($fornecedorDb->storeFornecedor(
                        $_POST['nome'], 
                        $_POST['frase'],
                        $_POST['email'],
                        $_POST['senha'],
                        $_POST['rua'],
                        $_POST['bairro'],
                        $_POST['cep'],
                        $_POST['complemento'],
                        $_POST['cidade'],
                        $_POST['estado'],
                        $_POST['latitude'],
                        $_POST['longitude'],
                        "F",
                         $_POST['cgc'],
                         $_POST['categoria'],
                         @str_replace(",", ".", str_replace(".", "", $_POST['valor_minimo'])),
                         $_POST['distancia'],
         @str_replace(",", ".", str_replace(".", "", $_POST['frete'])),
         $_POST['numero']                
        )){

    for($i=1;$i<=7;$i++){
     if(isset($_POST['d'.$i])){
  
         @$fornecedorDb->storeParametrosPeriodo(@$_POST['email'], $i,@$_POST['she'.$i].":".@$_POST['sme'.$i] , @$_POST['shs'.$i].":".@$_POST['sms'.$i], 'A'); 
     }
     else{
        
         @$fornecedorDb->storeParametrosPeriodo(@$_POST['email'], $i,'00:00' , '00:00', 'F');
     }
    }
    for($i=1;$i<=6;$i++){
     if(isset($_POST['p'.$i])){
  
         $fornecedorDb->storeParametrosPagamento(@$_POST['email'], $i); 
     }
     else{
        
 
     }
    }
    
    header("location: ../index.php?message=ok"); 
}
else {
    
    header("location: ../register.php?message=fail"); 
    
}
}
elseif($action=="exist"){

require_once '../dao/FornecedorDb.php';    

$fornecedorDb = new FornecedorModel();
    
    if($fornecedorDb->isUserExisted($_REQUEST['email'])){
        echo "{\"mail\": \"true\"}";
    }
    else{
        echo "{\"mail\": \"false\"}";
    }
    
}
elseif($action=="categoria"){
    require_once '../dao/FornecedorDb.php'; 
   $fornecedorDb = new FornecedorModel();
   $categoria = $fornecedorDb->getAllCategoria();
  echo json_encode($categoria);
}
elseif ($action=="logout") {
       setcookie("token", "", time()-3600,"/");
      setcookie("email", "", time()-3600,"/");
 header("location: ../index.php"); 

}
elseif ($action=="getallbydistance") {
       @$latitude = $_GET['latitude'];
      @$longitude =  $_GET['longitude'];
  
 require_once '../dao/FornecedorDb.php';
    $fornecedorDB = new FornecedorModel();
    $fornecedor = $fornecedorDB->getFornecedoresbyDistance($latitude, $longitude);
    
    echo json_encode($fornecedor);

}


?>
