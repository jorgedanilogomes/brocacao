<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

@$action = $_REQUEST['action'];

if ($action == "categoria") {
    require_once '../dao/ComplementoDB.php';
    $complementoDB = new ComplementoDB();
    $categoria = $complementoDB->getAllCategoria();
    echo json_encode($categoria);
} elseif ($action == "store") {
    @$id_fornecedor = $_COOKIE['id_fornecedor'];
    $categoria = @$_POST['categoria'];
    $valor = @str_replace(",", ".", str_replace(".", "", $_POST['valor']));
    $descricao = @$_POST['descricao'];

    if (empty($descricao) or empty($valor) or empty($categoria)
    ) {
        echo "{\"erro\": \"Existem campos não informados\"}";
    } else {
        //    return false;


     

     
        require_once '../dao/ComplementoDB.php';
        $complementoDB = new ComplementoDB();
        $complemento = $complementoDB->storeComplemento($id_fornecedor, $descricao, $categoria, $valor);
        if ($complemento) {
            echo "{\"erro\": \"false\"}";
        } else {
            //  return false;
            echo $complemento;
        }
    }
}

elseif ($action=="getall") {
      @$id_fornecedor = $_COOKIE['id_fornecedor'];
 require_once '../dao/ComplementoDB.php';
    $complementoDB = new ComplementoDB();
    $complemento = $complementoDB->getAllComplemento($id_fornecedor);
    echo json_encode($complemento);
    
}
elseif ($action=="get") {
      @$id_fornecedor = $_COOKIE['id_fornecedor'];
 require_once '../dao/ComplementoDB.php';
    @$id_complemento = $_REQUEST['id_complemento'];
    $complementoDB = new ComplementoDB();
    $complemento = $complementoDB->getComplemento($id_complemento,$id_fornecedor);
    echo json_encode($complemento);
    
}
elseif ($action=="delete") {
    require_once '../dao/ComplementoDB.php';
     @$id_fornecedor = $_COOKIE['id_fornecedor'];
     $complementoDB = new ComplementoDB();
     @$id_complemento = $_REQUEST['id_complemento'];
    $complemento = $complementoDB->deleteComplemento($id_complemento, $id_fornecedor);
    
    echo "{\"erro\": \"delete\"}";
    

}
elseif ($action=="edit") {
    
        @$id_fornecedor = $_COOKIE['id_fornecedor'];
        @$categoria = $_POST['categoria'];
        @$valor = str_replace(",", ".", str_replace(".", "", $_POST['valor']));
        @$descricao = $_POST['descricao'];
        @$id_complemento = $_POST['id_complemento'];
     
        require_once '../dao/ComplementoDB.php';
        $complementoDB = new ComplementoDB();
        $complemento = $complementoDB->updateComplemento($id_fornecedor, $descricao, $categoria, $valor,$id_complemento);
        if ($complemento) {
            echo "{\"erro\": \"false\"}";
        } else {
            //  return false;
            echo $complemento;
        }
}
