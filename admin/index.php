<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Registration Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <style>
            .linha-registro div{
                padding: 5px 5px !important;

            }
        </style>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
        <script src="assets/js/locationpicker.jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
        <link rel="stylesheet" href="http://jqueryvalidation.org/files/demo/site-demos.css">

        <link href="assets/css/toastr.css" rel="stylesheet"/>

        <script src="assets/js/toastr.min.js"></script>


        <?php
        if (@$_GET["message"] == "ok") {
            echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-top-full-width\"}      
toastr.info('Usuário registrado com sucesso!')
});</script>";
        }
        elseif (@$_GET["message"] == "fail") {
                     echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-top-full-width\"}      
toastr.info('Usuário ou senha invalidos!')
});</script>";

}
        ?>
    </head>

    <body>

        <!-- Top menu -->

        <!-- Top content -->



        <div class="container" style="margin-top: 10%">

            <div class="row linha-registro">
                <div class="col-md-4 col-lg-4"></div>
                <div class="col-md-4 col-lg-4 col-xs-12 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Login</h3>
                            <p>Faça seu login ou <a href="register.php">registre-se</a></p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-pencil"></i>
                        </div>
                    </div>
                    <div class="form-bottom" >

                        <form id="myform" role="form" action="index_api.php" method="post" class="registration-form" style="height: 300px;">
                                 <input type="hidden" name="controle" value="Fornecedor">
                            <input type="hidden" name="acao" value="login">

                            <div class="form-group">
                                <label class="sr-only" for="form-email">Informe seu email</label>
                                <input type="text" name="email" placeholder="Email" class="form-email form-control" id="email">
                                <span id="check"></span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-last-name">Senha</label>
                                <input type="password" name="senha" placeholder="Senha" class="form-first-name form-control" style="height: 50px;" id="senha" >
                            </div>




                            <button type="submit" class="btn " style="margin-left: 5px; margin-bottom: 5px; width: -moz-available; margin-right: 5px;">Entrar</button>
                        </form>
                    </div>
                </div>
                  <div class="col-md-4 col-lg-4"></div>
            </div>



        </div>


        <!-- Javascript -->

        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script>




            function updateControls(addressComponents) {
                var pais = addressComponents.country;
                var estado = addressComponents.stateOrProvince;
                var cidade = addressComponents.city;
                var bairro = addressComponents.district;
                var rua = addressComponents.addressLine1;
                var cep = addressComponents.postalCode;

                var ret = rua.split(" ");
                var rua ="";
                var tes = ret.length;


                for (var i = 1; i < tes; i++) {
                    rua +=ret[i]+" ";
                    // more statements
                }

                $('#us3-cidade').val(rua);
                $('#pais').val(pais);
                $('#estado').val(estado);
                $('#cidade').val(cidade);
                $('#bairro').val(bairro);
                $('#rua').val(rua);
                $('#cep').val(cep);
                document.getElementById("rua").disabled = true;
            }

            $('#us3').locationpicker({
                location: {latitude: -16.680757194312577, longitude: -49.25654875390626},
                radius: 1000,
                zoom: 15,
                inputBinding: {
                    latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    radiusInput: $('#radius'),
                    locationNameInput: $('#us3-address')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                    var addressComponents = $(this).locationpicker('map').location.addressComponents;


                    updateControls(addressComponents);
                    updateControls(addressComponents);
                }


            }



        );




        </script>
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
        <script>
            $(function () {


                // validate signup form on keyup and submit
                $("form").validate({
                    rules: {
                        email: {
                            required: true

                        },
                        senha: {
                            required: true

                        }


                    },
                    messages: {
                        senha: {
                            required: "Informe a senha"

                        },
                        email: {
                            required: "Informe o email"


                        }
                    }
                });
            });
        </script>

        <script>
            $(document).ready(function(){

                function explode(){
                    $("#us3-address").val("");
                }
                setTimeout(explode, 2000);



            });
        </script>



    </body>

</html>