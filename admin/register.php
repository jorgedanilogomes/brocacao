<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Registration Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <style>
            .linha-registro div{
                padding: 5px 5px !important;

            }
        </style>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
   <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyD8ycY-1IkXkwWmPmUcYVPx-r2eow-pkyA&libraries=places'></script>
        <script src="assets/js/locationpicker.jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
        <link rel="stylesheet" href="http://jqueryvalidation.org/files/demo/site-demos.css">
        <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
        <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal.css" />
        <link href="assets/css/toastr.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/css/jslider.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.blue.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.plastic.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.round.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.round.plastic.css" type="text/css">
        <!-- end -->



        <!-- bin/jquery.slider.min.js -->
        <script type="text/javascript" src="assets/js/jshashtable-2.1_src.js"></script>
        <script type="text/javascript" src="assets/js/jquery.numberformatter-1.2.3.js"></script>
        <script type="text/javascript" src="assets/js/tmpl.js"></script>
        <script type="text/javascript" src="assets/js/jquery.dependClass-0.1.js"></script>
        <script type="text/javascript" src="assets/js/draggable-0.1.js"></script>
        <script type="text/javascript" src="assets/js/jquery.slider.js"></script>
        <script src="assets/js/toastr.min.js"></script>
        <style type="text/css" media="screen">

            .layout { padding: 50px; font-family: Georgia, serif; }
            .layout-slider { margin-bottom:  60px; margin-top: 30px; width: 100%;  }
            .layout-slider-settings {  padding-bottom: 10px; }
            .layout-slider-settings pre { font-family: Courier;  }
            span {font-size: 15px;}
        </style>

        <?php
        if (@$_GET["message"] == "fail") {
            echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-top-full-width\"}      
toastr.info('Falha ao registrar usuário!')
});</script>";
        }
        ?>

    </head>

    <body>

        <!-- Top menu -->

        <!-- Top content -->



        <div class="container">

            <div class="row linha-registro">
                <div class="col-md-6 col-lg-6"></div>
                <div class="col-md-6 col-lg-6 col-xs-12 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Registre-se</h3>
                            <p>Faça seu registro2</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-pencil"></i>
                        </div>
                    </div>
                    <div class="form-bottom" >

                        <form id="myform" role="form" action="index_api.php" method="post" class="registration-form">
                           
                           <input type="hidden" name="controle" value="Fornecedor">
                            <input type="hidden" name="acao" value="storeFornecedor">
                            <div class="form-group">
                                <label class="sr-only" for="form-first-name">Nome do estabelecimento</label>
                                <input type="text" name="nome" placeholder="Nome do estabelecimento" class="form-first-name form-control" id="form-first-name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-first-name">Frase</label>
                                <input type="text" name="frase" placeholder="Informe uma frase de apresentação" maxlength="70" class="form-first-name form-control" id="frase">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Email</label>
                                <input type="text" name="email" placeholder="Email" class="form-email form-control" id="email">
                                <span id="check"></span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-last-name">Senha</label>
                                <input type="password" name="senha" placeholder="Senha" class="form-first-name form-control" id="senha" >
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-last-name">Confirme a senha</label>
                                <input type="password" name="senha2" placeholder="Confirme a senha" class="form-last-name form-control" id="senha2">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">CPF/CNPJ</label>
                                <select name="categoria" id="categoria" class="form-email form-control" >
                                    <option value='null'>Selecione uma categoria</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label class="sr-only" for="form-email">CPF/CNPJ</label>
                                <input type="text" name="cgc" placeholder="CPF ou CNPJ" id="cgc"class="cpf_cnpj form-email form-control" >
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-last-name">Informe seu endereço</label>
                                <input type="text" name="enderco" placeholder="Informe seu endereço" class="form-last-name form-control" id="us3-address">
                            </div>



                            <div class="form-group">
                                <button id='st' class="btn" style="width: 100%;" data-toggle="modal" data-target="#stack1" href="#stack1"><i class="fa fa-map-marker fa-2x"></i> Identifique sua localização</button>
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-email">Pais</label>
                                <input type="text" name="pais" placeholder="Pais" class="form-email form-control" id="pais">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Estado</label>
                                <input type="text" name="estado" placeholder="Estado" class="form-email form-control" id="estado">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Cep</label>
                                <input type="text" name="cep" placeholder="cep" class="form-email form-control" id="cep">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Cidade</label>
                                <input type="text" name="cidade" placeholder="Cidade" class="form-email form-control" id="cidade">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Bairro</label>
                                <input type="text" name="bairro" placeholder="Bairro" class="form-email form-control" id="bairro">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Rua</label>
                                <input type="text" name="rua" placeholder="Rua" class="form-email form-control" id="rua">
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-email">Latitude</label>
                                <input type="text" name="latitude" placeholder="Latitude" class="form-email form-control" id="latitude" >
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Longitude</label>
                                <input type="text" name="longitude" placeholder="Longitude" class="form-email form-control" id="longitude" >
                                <input type="hidden" name="radios"  id="radius" >
                            </div>
                             <div class="form-group">
                                <label class="sr-only" for="form-email">Numero</label>
                                <input  type="text" name="numero" placeholder="numero" id ="numero" class="form-email form-control" style="width: 100px;"">
                            </div>
                            
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Complemento</label>
                                <input  type="text" name="complemento" placeholder="Complemento" id ="complemento"class="form-email form-control" >
                            </div>
                            
                    <div class="form-group ">
                      <label style="padding-left: 5px;">Valor minimo do pedido</label><br>
                        <input class="money  form-email form-control" style="width: 100px; text-align: right;border: 1px;" placeholder="1,50" name="valor_minimo" id="valor_minimo" value="" type="text"   >
                    </div>
                           <div class="form-group ">
                      <label style="padding-left: 5px;">Valor do frete</label><br>
                        <input class="money  form-email form-control" style="width: 100px; text-align: right;border: 1px;" placeholder="1,50" name="frete" id="frete" value="" type="text"   >
                    </div>  
                            <div class="form-group white-bg"  >

                                <label style="padding-left: 5px;">Distância Maxima de Entrega</label>
                                <div class="layout-slider">


                                    <input id="distancia" type="slider" name="distancia" value="20" style="width: 100%;"/>
                                </div>

                                <script type="text/javascript" charset="utf-8">
                                    jQuery("#distancia").slider({from: 1, to: 50, step: 1.0, round: 1, format: {format: '##.0', locale: 'de'}, dimension: '&nbsp;KM', skin: "round"});
                                </script>      


                            </div>

                            <div class="form-group" >
                              
                                <table class="col-md-12 col-xs-12 table-bordered table-striped table-condensed cf">
                                    <thead class="cf">
                                        <tr>
                                            <th class="numeric">Dia</th>
                                            <th class="numeric">Aberto?</th>
                                            <th class="numeric">Inicio</th>
                                            <th class="numeric">Encerramento</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-title="Dia" class="numeric">Dom</td>
                                            <td data-title="Aberto?" class="numeric"><input type="checkbox"  name="d1" ></td>
                                            <td data-title="Inicio" class="numeric" >
                                                <select name="she1" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>

                                                </select>
                                                <select name="sme1" >
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>
                                            <td data-title="Encerramento" class="numeric" >
                                                <select name="shs1">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20" >20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>


                                                </select>
                                                <select name="sms1">
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>		

                                        </tr>

                                        <tr>
                                            <td data-title="Dia" class="numeric">Seg</td>
                                            <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d2"  ></td>
                                            <td data-title="Inicio" class="numeric" >
                                                <select name="she2" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>

                                                </select>
                                                <select name="sme2" >
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>
                                            <td data-title="Encerramento" class="numeric" >
                                                <select name="shs2">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>


                                                </select>
                                                <select name="sms2">
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>		

                                        </tr>
                                        <tr>
                                            <td data-title="Dia" class="numeric">Ter</td>
                                            <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d3"  ></td>
                                            <td data-title="Inicio" class="numeric" >
                                                <select name="she3" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>

                                                </select>
                                                <select name="sme3" >
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>
                                            <td data-title="Encerramento" class="numeric" >
                                                <select name="shs3">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>


                                                </select>
                                                <select name="sms3">
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>		

                                        </tr>
                                        <tr>
                                            <td data-title="Dia" class="numeric">Qua</td>
                                            <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d4"  ></td>
                                            <td data-title="Inicio" class="numeric" >
                                                <select name="she4" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>

                                                </select>
                                                <select name="sme4" >
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>
                                            <td data-title="Encerramento" class="numeric" >
                                                <select name="shs4">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>


                                                </select>
                                                <select name="sms4">
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>		

                                        </tr>
                                        <tr>
                                            <td data-title="Dia" class="numeric">Qui</td>
                                            <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d5"  ></td>
                                            <td data-title="Inicio" class="numeric" >
                                                <select name="she5" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>

                                                </select>
                                                <select name="sme5" >
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>
                                            <td data-title="Encerramento" class="numeric" >
                                                <select name="shs5">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>


                                                </select>
                                                <select name="sms5">
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>		

                                        </tr>
                                        <tr>
                                            <td data-title="Dia" class="numeric">Sex</td>
                                            <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d6"  ></td>
                                            <td data-title="Inicio" class="numeric" >
                                                <select name="she6" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>

                                                </select>
                                                <select name="sme6" >
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>
                                            <td data-title="Encerramento" class="numeric" >
                                                <select name="shs6">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>


                                                </select>
                                                <select name="sms6">
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>		

                                        </tr>
                                        <tr>
                                            <td data-title="Dia" class="numeric">Sab</td>
                                            <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d7"  ></td>
                                            <td data-title="Inicio" class="numeric" >
                                                <select name="she7" >
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>

                                                </select>
                                                <select name="sme7" >
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>
                                            <td data-title="Encerramento" class="numeric" >
                                                <select name="shs7">
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8" selected>08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>


                                                </select>
                                                <select name="sms7">
                                                    <option value="0">00</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>

                                            </td>		

                                        </tr>
                                        </body>
                                </table>
                                &nbsp;
                            </div>
                            <div class="form-group white-bg" style="height: 130px;" >
                                <div class="col-md-12 col-xs-12"> 
                                <label style="padding-left: 5px;">Formas de pagamento</label>
                                </div>
                                <div class="col-md-3 col-xs-2">
                                     <input type="checkbox"  name="p1" value='1' checked > &nbsp;<i class="fa fa-money" aria-hidden="true"></i> Dinheiro

                                    
                                </div>
                                 <div class="col-md-3 col-xs-2">
                                    <input type="checkbox"  name="p2" value='2' > &nbsp;<i class="fa fa-cc-mastercard" aria-hidden="true"></i>Master card


                                </div>
                                 <div class="col-md-3 col-xs-2">
                                    <input type="checkbox"  name="p3" value='3' > &nbsp;<i class="fa fa-cc-visa" aria-hidden="true"></i> Visa


                                </div>
                                
                               
                                
                                <div class="col-md-3 col-xs-2">
                                    <input type="checkbox"  name="p4" value='4'  > &nbsp;<img src="assets/img/sodexo.png" style="height: 12px; width: 18px;"> Sodexo

                                    
                                </div>

                                <div class="col-md-3 col-xs-2">
                                    <input type="checkbox"  name="p5" value='5'  > &nbsp;<img src="assets/img/ticket-logo-2.png" > Ticket

                                    
                                </div>
                                
                                 <div class="col-md-3 col-xs-2">
                                    <input type="checkbox"  name="p6" value='6'  > &nbsp;<img src="assets/ico/bandeira-elo.jpg" style="height: 16px; width: 16px;"> Elo

                                    
                                </div>
                            </div>
                            
                           
                             <div class="form-group" >

                                <button type="submit" class="btn" style="width: 100%;">Cadastrar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>

        <div id="stack1" class="modal " tabindex="-1" data-width="860"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Marque sua localização exata</h4>
                <div class="modal-body" style="padding: 0px">
                    <div class="row-fluid">

                        <div id="us3" class="teste"></div>          

                    </div>
                    <div class="modal-footer" style="padding: 0px">
                        <button type="button" data-dismiss="modal" class="btn">Confirmar</button>
                    </div>
                </div>
            </div>
            <!-- Javascript -->

            <script src="assets/bootstrap/js/bootstrap.min.js"></script>
            <script src="assets/js/jquery.backstretch.min.js"></script>
            <script src="assets/js/retina-1.1.0.min.js"></script>
            <script src="assets/js/scripts.js"></script>
            <script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
            <script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modal.js"></script>
            <script src="assets/js/jquery.mask.min.js" ></script>

            <script src="assets/js/valida_3.js"></script>
            <script src="assets/js/valida_cpf_cnpj.js"></script>
 
            <script>
$('.money').mask('000.000.000.000.000,00', {reverse: true});

                function updateControls(addressComponents) {
                    var pais = addressComponents.country;
                    var estado = addressComponents.stateOrProvince;
                    var cidade = addressComponents.city;
                    var bairro = addressComponents.district;
                    var rua = addressComponents.addressLine1;
                    var cep = addressComponents.postalCode;

                    var ret = rua.split(" ");
                    var rua = "";
                    var tes = ret.length;


                    for (var i = 1; i < tes; i++) {
                        rua += ret[i] + " ";
                        // more statements
                    }

                    $('#us3-cidade').val(rua);
                    $('#pais').val(pais);
                    $('#estado').val(estado);
                    $('#cidade').val(cidade);
                    $('#bairro').val(bairro);
                    $('#rua').val(rua);
                    $('#cep').val(cep);

                }











                $('#us3').locationpicker({
                    location: {latitude: -16.6766109, longitude: -49.275482499999995},
                    radius: 300,
                    zoom: 15,
                    radius: 1000,
                    inputBinding: {
                        latitudeInput: $('#latitude'),
                        longitudeInput: $('#longitude'),
                        radiusInput: $('#radius'),
                        locationNameInput: $('#us3-address')
                    },
                    enableAutocomplete: true,
                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                        // Uncomment line below to show alert on each Location Changed event
                        //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                        var addressComponents = $(this).locationpicker('map').location.addressComponents;


                        updateControls(addressComponents);
                        updateControls(addressComponents);
                    }
                });
                $('#stack1').on('shown.bs.modal', function () {
                    $('#us3').locationpicker('autosize');
                });



  
 





            </script>
            <!--[if lt IE 10]>
                <script src="assets/js/placeholder.js"></script>
            <![endif]-->
            <script>
                $(function () {

                    $.validator.addMethod(
                    "uniqueUserName",
                    function (value, element) {
                        $.ajax({
                            type: "POST",
                            url: "index_api.php?controle=Fornecedor&acao=ifExistMail",
                            data: "email=" + value,
                            dataType: "html",
                            success: function (msg)
                            {
                                var json = $.parseJSON(msg);
                                //If username exists, set response to true
                                if (json.mail == 'true') {
                                    response = false;
                                }
                                else {
                                    response = true;
                                }

                            }
                        });
                        return response;
                    },
                    "Já existe um usuário com este email!"
                );


                    $.validator.addMethod(
                    "cpfCnpj",
                    function (value, element) {
                              

                        // O CPF ou CNPJ
                        var cpf_cnpj = value;

                        // Testa a validação
                        if (valida_cpf_cnpj(cpf_cnpj)) {
                            response = true;
                        } else {
                            response = false;
                        }

                            
                        return response;
                    },
                    "CPF ou CNPJ inválidos!"
                );
                    
                    $.validator.addMethod(
                    "valCategoria",
                    function (value, element) {
                              

                        // O CPF ou CNPJ
                        var categoria = $("#categoria").val();

                        // Testa a validação
                        if (categoria=="null") {
                            response = false;
                        } else {
                            response = true;
                        }

                            
                        return response;
                    },
                    "Selecione uma categoria"
                ); 
                    
                

                    // validate signup form on keyup and submit
                    $("#myform").validate({
                        rules: {
                            senha: {
                                required: true,
                                minlength: 5
                            },
                            email: {
                                required: true,
                                email: true,
                                uniqueUserName: true
                            }
                            ,
                            senha2: {
                                required: true,
                                minlength: 5,
                                equalTo: "#senha"
                            },
                            complemento: {
                                required: true

                            },
                            cgc: {
                                cpfCnpj: true

                            },
                            categoria: {
                                valCategoria: true

                            }
                            ,
                            frase: {
                                required: true

                            },
                            numero: {
                                required: true

                            },
                            frete: {
                                required: true

                            },
                            valor_minimo: {
                                required: true

                            }

                        },
                        messages: {
                            senha: {
                                required: "Informe a senha",
                                minlength: "Sua senha deve possuir no minimo 5 caracteres"
                            },
                            senha2: {
                                required: "Informe a senha",
                                minlength: "Sua senha deve possuir no minimo 5 caracteres",
                                equalTo: "As senhas não estão iguais!"
                            },
                            complemento: {
                                required: "Este campo é obrigatório"
                            }
                        }
                    });
                });
            </script>

            <script>
                $(document).ready(function () {

                    function explode() {
                        $("#us3-address").val("");
                    }
                    setTimeout(explode, 2000);

                    $("#pais").prop('disabled', true);
                    $("#estado").prop('disabled', true);
                    $("#cidade").prop('disabled', true);
                    $("#bairro").prop('disabled', true);
                    $("#rua").prop('disabled', true);
                    $("#latitude").prop('disabled', true);
                    $("#longitude").prop('disabled', true);
                    $("#cep").prop('disabled', true);
                    
                    $.ajax({
                        url: "index_api.php?controle=Fornecedor&acao=getCategoriasByFornecedor",
                        //force to handle it as text
                        dataType: "json",
                        success: function(data) {
                        
                            $.each(data, function(i, value) {
                                $('#categoria').append($('<option>').text(value.descricao).attr('value', value.id_categoria_fornecedor));
                            });
                        }
                    });
                    
                    

                });
                $('#myform').submit(function () {
                    $("#pais").prop('disabled', false);
                    $("#estado").prop('disabled', false);
                    $("#cidade").prop('disabled', false);
                    $("#bairro").prop('disabled', false);
                    $("#rua").prop('disabled', false);
                    $("#latitude").prop('disabled', false);
                    $("#longitude").prop('disabled', false);
                    $("#cep").prop('disabled', false);



                });


                $('#email').keyup(function () {
                    this.value = this.value.toLowerCase();
                });

            </script>


    </body>

</html>