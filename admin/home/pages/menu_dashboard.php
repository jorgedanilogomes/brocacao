<div class="row" > 
    <div class="col-lg-3 col-md-6 listview-cardapio">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">26</div>
                        <div>Novos Pedidos</div>
                    </div>
                </div>
            </div>
            <a href="#lista" id="pedidos_novos">
                <div class="panel-footer">
                    <span class="pull-left">Visualisar</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 listview-cardapio">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">12</div>
                        <div>Pedidos em preparo</div>
                    </div>
                </div>
            </div>
            <a href="#lista" id="pedidos_preparo">
                <div class="panel-footer">
                    <span class="pull-left">Visualizar</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 listview-cardapio">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">124</div>
                        <div>Prontos para entrega</div>
                    </div>
                </div>
            </div>
            <a href="#lista" id="pedidos_prontos">
                <div class="panel-footer">
                    <span class="pull-left">Visualizar</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
      <a name="lista"></a>
    <div class="col-lg-3 col-md-6 listview-cardapio">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">13</div>
                        <div>Entregues</div>
                    </div>
                </div>
            </div>
            <a href="#lista" id="pedidos_entregues">
                <div class="panel-footer">
                    <span class="pull-left">Visualizar</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
  
</div>

<script>
    $("#pedidos_novos").click(function (){
        waitingDialog.show(); 
        $.ajax({
            url: "pedidos_novos.php",
            success: function(result){
                $("#bd").html(result);
            }});
     waitingDialog.hide();
    });

    $("#pedidos_preparo").click(function (){
        waitingDialog.show();   
        $.ajax({
            url: "pedidos_preparo.php",
            success: function(result){
                $("#bd").html(result);
            }});
        waitingDialog.hide();
    });
    
      $("#pedidos_entregues").click(function (){
         waitingDialog.show();  
        $.ajax({
            url: "pedidos_entregues.php",
            success: function(result){
                $("#bd").html(result);
            }});
       waitingDialog.hide();
    });
    
    $("#pedidos_prontos").click(function (){
          waitingDialog.show(); 
        $.ajax({
            url: "pedidos_prontos.php",
            success: function(result){
                $("#bd").html(result);
            }});
      waitingDialog.hide();
    });
</script> 