 

<?php
if (@$_GET["message"] == "ok") {
    echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-bottom-full-width\"}      
toastr.info('Complemento Cadastrado com Sucesso!')
});</script>";
} elseif(@$_GET["message"] == "deleted") {
     echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-bottom-full-width\"}      
toastr.info('Complemento Deletado com Sucesso!')
});</script>";
}
elseif(@$_GET["message"] == "notdeleted") {
     echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-bottom-full-width\"}      
toastr.info('Falha ao deletar Complemento!')
});</script>";
}
elseif(@$_GET["message"] == "edited") {
     echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-bottom-full-width\"}      
toastr.info('Registro alterado com sucesso!')
});</script>";
}
?>              

<div class="row">   

    <div class="col-md-6">

        <section class="task-panel tasks-widget">
            <div class="panel-heading">
                <div class="pull-left"><h5><i class="fa fa-tasks"></i> Lista de complementos  </h5></div> <div class="pull-right"> <a class="btn btn-success btn-sm" id="novo" ><i class="fa fa-plus"></i>
                        cadastrar</a> </div>
                <br>
            </div>
            <div class="panel-body">
                <div class="task-content">

                    <ul class="task-list" id="complementobody" style="list-style: none;">
                       
                    </ul>
                </div>

                <div class=" add-task-row">

                </div>
            </div>
        </section>
    </div>
</div>
<script>
   $(document).ready(function(){
		
        
     
            
                $.ajax({
        url: "../../index_api.php?acao=getAllComplemento&controle=Complemento",
        //force to handle it as text
        dataType: "json",
        success: function (data) {

            $.each(data, function (i, value) {
              //  $('#categoria').append($('<option>').text(value.descricao).attr('value', value.id_categoria_fornecedor));
             var descricao = value.descricao;
                var complemento = '<li >' +
                            '<div class="task-title">'+ 
                                '<span class="task-title-sp">'+value.descricao+'</span>'+
                                '<span class="badge bg-theme">'+value.categoria+'</span>'+
                                 '<span class="badge bg-theme">R$ '+value.valor+'</span>'+
                                '<div class="pull-right hidden-phone">'+

                                   '<button class="btn btn-primary btn-xs" onclick="editar('+value.id_complemento+')"><i class="fa fa-pencil fa-lg"></i></button>'+
                                    ' <button class="btn btn-danger btn-xs" onclick="deletar('+value.id_complemento+')"><i class="fa fa-trash-o fa-lg"></i></button>'+
                                    '<input type="hidden" >' +
                                '</div>'+
                            '</div>'+
                        '</li>';
               $("#complementobody").append(complemento);
            });
        }
    });
        
    }); 
 
    
    $("#novo").click(function (){
         waitingDialog.show();
        $.ajax({
            url: "form_complemento.php",
            success: function(result){
                $("#bd").html(result);
            }});
      waitingDialog.hide();
    });
    
    
 
  
            
            
</script>        

<script>
       function deletar(id_complemento){
            waitingDialog.show();
        $.ajax({
            url: "../../index_api.php?acao=deleteComplemento&controle=complemento&id_complemento="+id_complemento,
            success: function(result){
               
              var json = $.parseJSON(result);
               if (json.erro == 'delete'){
                   $.ajax({
                    url: "lista_complementos.php?message=deleted",
                    success: function(result){
                    $("#bd").html(result);
                    }});
                    $('#hd').text(" "); 
               } 
               else{
                     $.ajax({
                    url: "lista_complementos.php?message=notdeleted",
                    success: function(result){
                    $("#bd").html(result);
                    }});
                    $('#hd').text(" ");
               } 
            }});
         waitingDialog.hide();
    }
    
    function editar(id_complemento){
         waitingDialog.show();
        $.ajax({
                    url: "form_complemento.php?action=edit&id_complemento="+id_complemento,
                    success: function(result){
                    $("#bd").html(result);
                    }});
                
                 waitingDialog.hide();
                    $('#hd').text(" ");
    }

      waitingDialog.hide();
</script>    