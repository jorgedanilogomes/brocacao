 

<?php
if (@$_GET["message"] == "ok") {
    echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-bottom-full-width\"}      
toastr.info('Registro gravado com sucesso!!')
});</script>";
} elseif(@$_GET["message"] == "deleted") {
     echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-bottom-full-width\"}      
toastr.info('Complemento Deletado com Sucesso!')
});</script>";
}
elseif(@$_GET["message"] == "notdeleted") {
     echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-bottom-full-width\"}      
toastr.info('Falha ao deletar Complemento!')
});</script>";
}

?>               




<div class="row" id="cardapiobody" >   
    <div class="col-lg-12 " style="padding-bottom: 5px;">
                   <a class="btn btn-success btn-sm" id="novo" ><i class="fa fa-plus"></i>
                        cadastrar</a>
    </div>
                    
                    
                 
              
                   
                <!-- /.row -->
            
            <!-- /#page-wrapper -->
                </div>

<script>
          $(document).ready(function(){
		
      
                 
                $.ajax({
                  
        url: "../../index_api.php?acao=getall&controle=cardapio",
        //force to handle it as text
        dataType: "json",
        success: function (data) {

            $.each(data, function (i, value) {
              //  $('#categoria').append($('<option>').text(value.descricao).attr('value', value.id_categoria_fornecedor));
             
                var complemento =    
                        '<div class="col-lg-6 listview-cardapio">'+
                             '<div class="panel panel-primary">' +
                            ' <div class="panel-heading panel-heading-list">' +
                                '<div class="pull-left edit-left">' +
                                value.nome +
                            '</div>'+
                           ' <div class="pull-right edit-right">'+
                                '<button class="btn btn-primary btn-xs" onclick="editar('+value.id_cardapio+')"><i class="fa fa-pencil"></i></button>'+
                                 '<button class="btn btn-danger btn-xs" onclick="deletar('+value.id_cardapio+')"><i class="fa fa-trash-o fa-lg"></i></button>'+
                             
                                        '</div>'+
                            '</div>'+
                            '<div class="panel-body">'+
                                '<p>' + value.descricao + '</p>' +
                            '</div>' +
                            '<div class="panel-footer">'+
                                   '<span><i class="fa fa-clock-o"></i> ' +  value.horas +  'H '+ value.minutos+ 'M </span><span>&nbsp;&nbsp;<i class="fa fa-money"></i> R$' + value.valor + '</span> '+
                            '</div>'+
                        '</div>' +
                    '</div>';
               $("#cardapiobody").append(complemento);
            });
        }
    });
    }); 
        

    
        function editar(id_cardapio){
                 waitingDialog.show();
        $.ajax({
                    url: "form_cardapio.php?action=edit&id_cardapio="+id_cardapio,
                    success: function(result){
                    $("#bd").html(result);
                    }});
                 waitingDialog.hide();
                    $('#hd').text(" ");
    }
    
      function deletar(id_cardapio){
           waitingDialog.show();
        $.ajax({
            url: "../../index_api.php?acao=delete&controle=cardapio&id_cardapio="+id_cardapio,
            success: function(result){
               
              var json = $.parseJSON(result);
               if (json.erro == 'delete'){
                   $.ajax({
                    url: "lista_cardapios.php?message=deleted",
                    success: function(result){
                    $("#bd").html(result);
                    }});
                    $('#hd').text(" "); 
               } 
               else{
                     $.ajax({
                    url: "lista_cardapios.php?message=notdeleted",
                    success: function(result){
                    $("#bd").html(result);
                    }});
                    $('#hd').text(" ");
               } 
            }});
  
         waitingDialog.hide();
    }
    $("#novo").click(function (){
         waitingDialog.show();
        $.ajax({
            url: "form_cardapio.php",
            success: function(result){
                $("#bd").html(result);
            }});
      waitingDialog.hide();
    });  
    </script>
    <script>
            waitingDialog.hide();
        </script>