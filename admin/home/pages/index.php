<?php require 'template.php'; ?>
    <body>

        <div id="wrapper">
            <?php
            include 'menu.html';
            ?>
            <!-- Navigation -->
            <!-- /.row -->
               <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header" id="hd">Dashboard</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row" id="bd" style="margin: 1px">
                   
                    
                       
                        
                </div>
                <!-- /.row -->
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../js/metisMenu.min.js"></script>
 <script src="../js/jquery.mask.min.js" ></script>


    <!-- Custom Theme JavaScript -->
    <script src="../js/startmin.js"></script>
    <script> 
        $(document).ready(function(){
		waitingDialog.show();
        $.ajax({
                    url: "pedidos_novos.php",
                    success: function(result){
                    $("#bd").html(result);
                }});
		
		waitingDialog.hide();
            });
        
        $("#dashboard").click(function (){
         waitingDialog.show();
            $.ajax({
                    url: "pedidos_novos.php",
                    success: function(result){
                    $("#bd").html(result);
                }});
     $(".navbar-toggle").click();
     waitingDialog.hide();
        });
        
  
     
        
         
        
        
        $("#form_cardapio").click(function (){
         waitingDialog.show();
            $.ajax({
                    url: "form_cardapio.php",
                    success: function(result){
                    $("#bd").html(result);
                }});
            $('#hd').text("Cadastro de cardapio");
             $(".navbar-toggle").click();
             waitingDialog.hide();
        });
        
             $("#config").click(function (){
         waitingDialog.show();
            $.ajax({
                    url: "form_parametros.php",
                    success: function(result){
                    $("#bd").html(result);
                }});
            $('#hd').text("");
            
             waitingDialog.hide();
        });
            
     
        
        
        $("#lista_cardapio").click(function (){
         waitingDialog.show();
            $.ajax({
                    url: "lista_cardapios.php",
                    success: function(result){
                    $("#bd").html(result);
                }});
            $('#hd').text("Lista Cardapio de Produtos");
             $(".navbar-toggle").click();
             waitingDialog.hide();
        }); 
        
           $("#form_complemento").click(function (){
             waitingDialog.show();
            $.ajax({
                    url: "form_complemento.php",
                    success: function(result){
                    $("#bd").html(result);
                }});
            $('#hd').text("Cadastro de Complemento");
             $(".navbar-toggle").click();
             waitingDialog.hide();
        }); 
        
           $("#lista_complemento").click(function (){
         waitingDialog.show();
            $.ajax({
                    url: "lista_complementos.php",
                    success: function(result){
                    $("#bd").html(result);
                }});
            $('#hd').text(" ");
             $(".navbar-toggle").click();
             waitingDialog.hide();
        }); 
             
             

    </script>
    
    <script>
        var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);
        
          waitingDialog.hide();
        </script>
         
</body>
</html>


