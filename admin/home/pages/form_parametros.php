        
<link rel="stylesheet" href="../css/jslider.css" type="text/css">
<link rel="stylesheet" href="../css/jslider.blue.css" type="text/css">
<link rel="stylesheet" href="../css/jslider.plastic.css" type="text/css">
<link rel="stylesheet" href="../css/jslider.round.css" type="text/css">
<link rel="stylesheet" href="../css/jslider.round.plastic.css" type="text/css">
<!-- end -->



<!-- bin/jquery.slider.min.js -->
<script type="text/javascript" src="../js/jshashtable-2.1_src.js"></script>
<script type="text/javascript" src="../js/jquery.numberformatter-1.2.3.js"></script>
<script type="text/javascript" src="../js/tmpl.js"></script>
<script type="text/javascript" src="../js/jquery.dependClass-0.1.js"></script>
<script type="text/javascript" src="../js/draggable-0.1.js"></script>
<script type="text/javascript" src="../js/jquery.slider.js"></script>


<style type="text/css" media="screen">

    .layout { padding: 50px; font-family: Georgia, serif; }
    .layout-slider { margin-bottom:  60px; margin-top: 30px; width: 50%;  }
    .layout-slider-settings {  padding-bottom: 10px; }
    .layout-slider-settings pre { font-family: Courier;  }
    span {font-size: 15px;}
</style>

<div class="col-lg-12">
    <div class="panel panel-default">

        <div class="panel-body">
            <div class="col-md-12">
                <form action="../../control/cardapio.php" method="" id="cardapio" method="post">
                    <input type="hidden" id="id_fornecedor" name="id_cardapio" value="<?php echo @$_GET['id_fornecedor'] ?>">


                    <div class="form-group">
                        <label>Valor Minimo do Pedido R$</label>
                        <input class="money form-control" style="width: 100px; text-align: right" placeholder="1,50" name="valor" id="valor" value="" >
                    </div>

                    <div class="form-group">
                        <label>Distância Maxima de Entrega</label>
                        <div class="layout-slider">
                            <input id="distancia" type="slider" name="distancia" value="20" style="width: 100%;"/>
                        </div>

                        <script type="text/javascript" charset="utf-8">
                            jQuery("#distancia").slider({from: 1, to: 50, step: 1.0, round: 1, format: {format: '##.0', locale: 'de'}, dimension: '&nbsp;KM', skin: "round"});
                        </script>      

                    </div>


            </div>  
            <div class="row">
                <div class="col-md-7">
                    <div class="row">

                        <div id="no-more-tables">
                            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                <thead class="cf">
                                    <tr>
                                        <th class="numeric">Dia</th>
                                        <th class="numeric">Aberto?</th>
                                        <th class="numeric">Inicio</th>
                                        <th class="numeric">Encerramento</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-title="Dia" class="numeric">Dom</td>
                                        <td data-title="Aberto?" class="numeric"><input type="checkbox"  name="d1" ></td>
                                        <td data-title="Inicio" class="numeric" >
                                            <select name="she1" >
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>

                                            </select>
                                            <select name="sme1" >
                                                <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>
                                        <td data-title="Encerramento" class="numeric" >
                                            <select name="shs1">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20" >20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>


                                            </select>
                                            <select name="sms1">
                                                    <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>		

                                    </tr>
                                    
                                    <tr>
                                        <td data-title="Dia" class="numeric">Seg</td>
                                        <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d2"  ></td>
                                        <td data-title="Inicio" class="numeric" >
                                            <select name="she2" >
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>

                                            </select>
                                            <select name="sme2" >
                                                <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>
                                        <td data-title="Encerramento" class="numeric" >
                                            <select name="shs2">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>


                                            </select>
                                            <select name="sms2">
                                                    <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>		

                                    </tr>
<tr>
                                        <td data-title="Dia" class="numeric">Ter</td>
                                        <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d3"  ></td>
                                        <td data-title="Inicio" class="numeric" >
                                            <select name="she3" >
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>

                                            </select>
                                            <select name="sme3" >
                                                <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>
                                        <td data-title="Encerramento" class="numeric" >
                                            <select name="shs3">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>


                                            </select>
                                            <select name="sms3">
                                                    <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>		

                                    </tr>
<tr>
                                        <td data-title="Dia" class="numeric">Qua</td>
                                        <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d4"  ></td>
                                        <td data-title="Inicio" class="numeric" >
                                            <select name="she4" >
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>

                                            </select>
                                            <select name="sme4" >
                                                <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>
                                        <td data-title="Encerramento" class="numeric" >
                                            <select name="shs4">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>


                                            </select>
                                            <select name="sms4">
                                                    <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>		

                                    </tr>
                                    <tr>
                                        <td data-title="Dia" class="numeric">Qui</td>
                                        <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d5"  ></td>
                                        <td data-title="Inicio" class="numeric" >
                                            <select name="she5" >
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>

                                            </select>
                                            <select name="sme5" >
                                                <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>
                                        <td data-title="Encerramento" class="numeric" >
                                            <select name="shs5">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>


                                            </select>
                                            <select name="sms5">
                                                    <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>		

                                    </tr>
                                      <tr>
                                        <td data-title="Dia" class="numeric">Sex</td>
                                        <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d6"  ></td>
                                        <td data-title="Inicio" class="numeric" >
                                            <select name="she6" >
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>

                                            </select>
                                            <select name="sme6" >
                                                <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>
                                        <td data-title="Encerramento" class="numeric" >
                                            <select name="shs6">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>


                                            </select>
                                            <select name="sms6">
                                                    <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>		

                                    </tr>
  <tr>
                                        <td data-title="Dia" class="numeric">Sab</td>
                                        <td data-title="Aberto?" class="numeric"><input type="checkbox" name="d7"  ></td>
                                        <td data-title="Inicio" class="numeric" >
                                            <select name="she7" >
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>

                                            </select>
                                            <select name="sme7" >
                                                <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>
                                        <td data-title="Encerramento" class="numeric" >
                                            <select name="shs7">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                 <option value="8" selected>08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>


                                            </select>
                                            <select name="sms7">
                                                    <option value="0">00</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>

                                        </td>		

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>            
            </div>     
            <div class="row">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-trash" style="margin-top: 10px;">Gravar</button>
                </div>
            </div>
        </div><!-- /.col-->
    </div>
</div>
</div>

<div class="container">
