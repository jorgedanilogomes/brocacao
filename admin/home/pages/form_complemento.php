

<form id="complemento" action="../../index_api.php" method="post">

<div class="col-lg-12">
    <div class="panel panel-default">
        <input type="hidden" id="id_complemento" name="id_complemento" value="<?php echo @$_GET['id_complemento']  ?>">
        <input type="hidden" name="acao" value="storeComplemento">
        <input type="hidden" name="controle" value="complemento">
        <div class="panel-body">
            <div class="col-md-6">
                

                    <div class="form-group">
                        <label>Descrição do complemento</label>
                        <input class="form-control" placeholder="Bacon" name="descricao" id="descricao" value="">
                    </div>
                    <div class="form-group">
                        <label>Categoria</label>
                        <select id = "categoria" name = "categoria" class="form-control">
                           
                        </select>
                    </div>
                 <div class="form-group">
                        <label>Valor R$</label>
                        <input class="money form-control" style="width: 100px; text-align: right" placeholder="1,50" name="valor" id="valor" value="" >
                        <input type="hidden" name="action" value="store" >
                    </div>

                  	<div class="form-group">
                        <button type="submit" class="btn btn-primary btn-trash">Gravar</button>
                    </div>					
            </div>

            
<div id="result"></div>
          
        </div>
    </div>
</div><!-- /.col-->
</div>
  </form>
<!-- /.col-lg-12 -->


<script>
      
    $(document).ready(function(){
    
           
        $.ajax({
        url: "../../index_api.php?acao=getAll&controle=categoria",
        //force to handle it as text
        dataType: "json",
        success: function (data) {

            $.each(data, function (i, value) {
                $('#categoria').append($('<option>').text(value.descricao).attr('value', value.id_categoria));
            });
        }
    });
       
  
 $('.money').mask('000.000.000.000.000,00', {reverse: true});


 if($("#complemento" ).find("input[name='id_complemento']" ).val()!=''){
         
       $.ajax({
                    url: "../../index_api.php?acao=getComplemento&controle=complemento&id_complemento="+$('#id_complemento').val(),
                    //force to handle it as text
                dataType: "text",
                    success: function(data) {
                        
                        //data downloaded so we call parseJSON function 
                        //and pass downloaded data
                
                        
                        var json = $.parseJSON(data);
                            
                        //now json variable contains data in json format
                        //let's display a few items
                              $( "#complemento" ).find("input[name='descricao']" ).attr('value',json[0].descricao);
                              $( "#complemento" ).find("select[name='categoria']" ).find("[value="+json[0].id_categoria+"]" ).attr("selected","selected");
                              $( "#complemento" ).find("input[name='valor']" ).attr('value',json[0].valor);                 
        // $("select option[value='"+json[0].id_categoria+"']").attr("selected","selected");
                             
                    }
                }); 
                  
  }



//Testa se é para editar ou gravar um novo


//graga os dados

$( "#complemento" ).submit(function( event ) {
   waitingDialog.show();
  // Stop form from submitting normally
  event.preventDefault();
 
  // Get some values from elements on the page:
  var $form = $( this ),
    cat = $form.find( "select[name='categoria']" ).val(),
    desc = $form.find( "input[name='descricao']" ).val(),
    val = $form.find( "input[name='valor']" ).val(),
    act = $form.find( "input[name='acao']" ).val(),
    ctl = $form.find( "input[name='controle']" ).val(),
    id = $form.find( "input[name='id_complemento']" ).val(),
    url = $form.attr( "action" );
    
 
  // Send the data using post
  
  
  var posting = $.post( url, { categoria : cat , descricao : desc , valor : val , acao : act, controle : ctl, id_complemento : id  } );
 
  // Put the results in a div
  posting.done(function( data ) {
  var json = $.parseJSON(data);
  
  if(json.erro == 'false'){
     
     $.ajax({
            url: "lista_complementos.php?message=ok",
            success: function(result){
                $("#bd").html(result);
            }});
        
        $('#hd').text(" ");
    }
    else{
 
     toastr.options = {"positionClass": "toast-bottom-full-width"}   ;   
     toastr.info(json.erro);
    }
  });
      waitingDialog.hide();
});

});
 
</script>