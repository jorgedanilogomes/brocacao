

<div class="col-lg-12">
    <div class="panel panel-default">

        <div class="panel-body">
            <div class="col-md-6">
                <form action="../../index_api.php" method="" id="cardapio" method="post">
                    
                    <input type="hidden" id="id_cardapio" name="id_cardapio" value="<?php echo @$_GET['id_cardapio']  ?>">
                     <input type="hidden" name="acao" value="store" >
                     <input type="hidden" name="controle" value="cardapio" >
                    <div class="form-group">
                        <label>Nome do Prato</label>
                        <input class="form-control" placeholder="X-Tudo" name="nome" id="nome">
                    </div>
                    <div class="form-group">
                        <label>Tempo de Preparo</label>

                        <div class="row">

                            <div class="col-md-3">
                                Horas : 
                            </div> 
                            <div class="col-md-3">
                                <select name="horas" class="form-control">
                                    <option value="0">0</option>
                                    <option value="60">1</option>
                                    <option value="120">2</option>
                                    <option value="180">3</option>
                                    <option value="240">4</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                Minutos 
                            </div>
                            <div class="col-md-3">
                                <select name="minutos" class="form-control">
                                    <option value="0">0</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                            </div>
                        </div>  <!-- /--row -->  
                        <div class="form-group">
                            <label>Descrição / Ingredientes</label>
                            <textarea  name="descricao" class="form-control" rows="3" placeholder="Descreva aqui os ingredientes para preparo deste prato"></textarea>
                        </div>
                        <div class="form-group">
                        <label>Categoria</label>
                        <select id = "categoria" name = "categoria" class="form-control">
                           
                        </select>
                    </div>
                         <div class="form-group">
                        <label>Valor R$</label>
                        <input class="money form-control" style="width: 100px; text-align: right" placeholder="1,50" name="valor" id="valor" value="" >
                      
                    </div>
               

                    <div class="row">	
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-trash">GRAVAR</button>
                           
                    </div>

                </form>
                    
            </div>
        </div>
    </div><!-- /.col-->
</div>
<!-- /.col-lg-12 -->

<script>
      
    $(document).ready(function(){
    
          
        $.ajax({
        url: "../../index_api.php?acao=getAll&controle=categoria",
        //force to handle it as text
        dataType: "json",
        success: function (data) {

            $.each(data, function (i, value) {
                $('#categoria').append($('<option>').text(value.descricao).attr('value', value.id_categoria));
            });
        }
    });
      
  
 $('.money').mask('000.000.000.000.000,00', {reverse: true});
 
    });
    //alert($("#cardapio" ).find("input[name='id_cardapio']" ).val());
    if($("#cardapio" ).find("input[name='id_cardapio']" ).val()!=''){
         
       $.ajax({
                    url: "../../index_api.php?acao=get&controle=cardapio&id_cardapio="+$('#id_cardapio').val(),
                    //force to handle it as text
                dataType: "text",
                    success: function(data) {
                        
                        //data downloaded so we call parseJSON function 
                        //and pass downloaded data
                
                        
                        var json = $.parseJSON(data);
                            
                        //now json variable contains data in json format
                        //let's display a few items
                              $( "#cardapio" ).find("textarea[name='descricao']" ).text(json[0].descricao);
                              $( "#cardapio" ).find("input[name='nome']" ).attr('value',json[0].nome);
                              $( "#cardapio" ).find("select[name='horas']" ).find("[value="+json[0].horas*60+"]" ).attr("selected","selected");
                              $( "#cardapio" ).find("select[name='minutos']" ).find("[value="+json[0].minutos+"]" ).attr("selected","selected");
                              $( "#cardapio" ).find("select[name='categoria']" ).find("[value="+json[0].id_categoria+"]" ).attr("selected","selected");
                              $( "#cardapio" ).find("input[name='valor']" ).attr('value',json[0].valor);                 
        // $("select option[value='"+json[0].id_categoria+"']").attr("selected","selected");
                             
                    }
                }); 
                  
  }
    $( "#cardapio" ).submit(function( event ) {
 waitingDialog.show();
  // Stop form from submitting normally
  event.preventDefault();
 
  
 
 
  // Get some values from elements on the page:
  var $form = $( this ),
    id = $form.find( "input[name='id_cardapio']" ).val(),
    cat = $form.find( "select[name='categoria']" ).val(),
    desc = $form.find( "textarea[name='descricao']" ).val(),
    val = $form.find( "input[name='valor']" ).val(),
    act = $form.find( "input[name='action']" ).val(),
    nam = $form.find( "input[name='nome']" ).val(),
    min = $form.find( "select[name='minutos']" ).val(),
    hor = $form.find( "select[name='horas']" ).val(),
    url = $form.attr( "action" );
 
  // Send the data using post
  
  
  var posting = $.post( url, { categoria : cat , descricao : desc , valor : val , action : act , nome : nam , minutos : min , horas : hor , id_cardapio : id , acao : "store", controle : "cardapio" } );
 
  // Put the results in a div
  posting.done(function( data ) {
  var json = $.parseJSON(data);
  
  if(json.erro == 'false'){
     
            $.ajax({
            url: "lista_cardapios.php?message=ok",
            success: function(result){
                $("#bd").html(result);
            }});
        
        $('#hd').text(" ");
    }
    else{
 
     toastr.options = {"positionClass": "toast-bottom-full-width"}   ;   
     toastr.info(json.erro);
    }
  });
     waitingDialog.hide();
});

  waitingDialog.hide();
</script>