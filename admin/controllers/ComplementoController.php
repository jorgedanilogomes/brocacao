<?php

require_once 'models/ComplementoModel.php';

/**
 * Controlador responsável por gerenciar o
 * fluxo de dados entre a camada de modelo e a de visão
 *
 */
class ComplementoController {

    protected $o_complemento;

    /**
     * Construtor padrão responsável por novas instâncias dessa classe.
     *
     * FornecedorController constructor.
     */
    public function __construct() {

        $this->o_complemento = new ComplementoModel();
    }

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

  public  function storeComplementoAction() {

        @$id_fornecedor = $_COOKIE['id_fornecedor'];
        $categoria = @$_POST['categoria'];
        $valor = @str_replace(",", ".", str_replace(".", "", $_POST['valor']));
        $descricao = @$_POST['descricao'];
        @$id_complemento = $_POST['id_complemento'];

        if (empty($descricao) or empty($valor) or empty($categoria)
        ) {
            echo "{\"erro\": \"Existem campos não informados\"}";
        } else {

            if (empty($id_complemento)) {

                $complemento = $this->o_complemento->storeComplemento($id_fornecedor, $descricao, $categoria, $valor);
                if ($complemento) {
                    echo "{\"erro\": \"false\"}";
                } else {
                    //  return false;
                    echo $complemento;
                }
            } else {
                $this->editComplementoAction();
            }
        }
    }

 public   function getAllComplementoAction() {

        @$id_fornecedor = $_COOKIE['id_fornecedor'];
        $complemento = $this->o_complemento->getAllComplemento($id_fornecedor);
        echo json_encode($complemento);
    }

 public   function getComplementoAction() {
        @$id_fornecedor = $_COOKIE['id_fornecedor'];

        @$id_complemento = $_REQUEST['id_complemento'];

        $complemento = $this->o_complemento->getComplemento($id_complemento, $id_fornecedor);
        echo json_encode($complemento);
    }

  public  function deleteComplementoAction() {

        @$id_fornecedor = $_COOKIE['id_fornecedor'];

        @$id_complemento = $_REQUEST['id_complemento'];
        $complemento = $this->o_complemento->deleteComplemento($id_complemento, $id_fornecedor);

        echo "{\"erro\": \"delete\"}";
    }

 public   function editComplementoAction() {

        @$id_fornecedor = $_COOKIE['id_fornecedor'];
        @$categoria = $_POST['categoria'];
        @$valor = str_replace(",", ".", str_replace(".", "", $_POST['valor']));
        @$descricao = $_POST['descricao'];
        @$id_complemento = $_POST['id_complemento'];

        $complemento = $this->o_complemento->updateComplemento($id_fornecedor, $descricao, $categoria, $valor, $id_complemento);
        if ($complemento) {
            echo "{\"erro\": \"false\"}";
        } else {
            //  return false;
            echo $complemento;
        }
    }

}

