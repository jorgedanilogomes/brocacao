<?php

require_once 'models/CategoriaModel.php';

/**
 * Controlador responsável por gerenciar o
 * fluxo de dados entre a camada de modelo e a de visão
 *
 */
class CategoriaController {

    protected $o_categoria;

    /**
     * Construtor padrão responsável por novas instâncias dessa classe.
     *
     * FornecedorController constructor.
     */
    public function __construct() {

        $this->o_categoria = new CategoriaModel();
    }


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public function getAllAction(){
  
    $categoria = $this->o_categoria->getAllCategoria();
    echo json_encode($categoria);
    
}



}

