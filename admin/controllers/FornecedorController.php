<?php

require_once 'models/FornecedorModel.php';

/**
 * Controlador responsável por gerenciar o
 * fluxo de dados entre a camada de modelo e a de visão
 *
 */
class FornecedorController {

    protected $o_fornecedor;

    /**
     * Construtor padrão responsável por novas instâncias dessa classe.
     *
     * FornecedorController constructor.
     */
    public function __construct() {

        $this->o_fornecedor = new FornecedorModel();
    }

    /**
     * Metodo responsável por listar todas as categorias
     */
    public function categoriaAction() {

        @$latitude = $_GET['latitude'];

        @$longitude = $_GET['longitude'];

        $categoria = $this->o_fornecedor->getAllCategoria();

        echo json_encode($categoria);
    }

    /**
     * Método responsável por buscar os fornecedores pelas suas distâncias.
     */
    public function buscaFornecedorByDistanceAction() {

        $data = json_decode(file_get_contents("php://input"));

        @$latitude = $data->latitude;

        @$longitude = $data->longitude;

        $fornecedor = $this->o_fornecedor->getFornecedoresbyDistance($latitude, $longitude);

        echo json_encode($fornecedor);
    }

    /**
     * Método responsável por buscar os cardapios dos fornecedores passado por parametro.
     *
     */
    public function buscaCardapioAction() {

        @$id = base64_decode($_COOKIE['fornecedor']);

        $cardapio = $this->o_fornecedor->getCardapioFornecedor($id);

        echo json_encode($cardapio);
    }

    /**
     * Método responsável por buscar o Cardapio por id
     *
     */
    public function buscaCardapioByIdAction() {

        @$id_fornecedor = base64_decode($_COOKIE['fornecedor']);

        @$id_cardapio = $_GET['id_cardapio'];

        if (DataValidator::isNumeric($id_fornecedor) || DataValidator::isNumeric($id_cardapio)) {

            $resultado = $this->o_fornecedor->getCardapiobyId($id_cardapio, $id_fornecedor);
        }

        echo json_encode($resultado);
    }

    /**
     * Método responsável por Buscar fornecedor pelo seu id.
     */
    public function getforbyidAction() {

        $data = json_decode(file_get_contents("php://input"));

        @$latitude = $data->latitude;

        @$longitude = $data->longitude;

        @$id = base64_decode($_COOKIE['fornecedor']);

        $fornecedor = $this->o_fornecedor->getFornecedor($latitude, $longitude, $id);

        echo json_encode($fornecedor);
    }

    //TODO: essa função deverá ser feita no angular.
    public function listtotpedAction() {

        $arr = json_decode(base64_decode($_COOKIE['p__dkv']), true);

        $narr = $arr[base64_decode($_COOKIE['fornecedor'])];

        echo json_encode($narr);
    }

    public function ifExistMailAction() {



        if ($this->o_fornecedor->isUserExisted($_REQUEST['email'])) {
            echo "{\"mail\": \"true\"}";
        } else {
            echo "{\"mail\": \"false\"}";
        }
    }

    public function getCategoriasByFornecedorAction() {


        $categoria = $this->o_fornecedor->getAllCategoria();
        echo json_encode($categoria);
    }

    public function storeFornecedorAction() {

        if ($this->o_fornecedor->storeFornecedor(
                        $_POST['nome'], $_POST['frase'], $_POST['email'], $_POST['senha'], $_POST['rua'], $_POST['bairro'], $_POST['cep'], $_POST['complemento'], $_POST['cidade'], $_POST['estado'], $_POST['latitude'], $_POST['longitude'], "F", $_POST['cgc'], $_POST['categoria'], @str_replace(",", ".", str_replace(".", "", $_POST['valor_minimo'])), $_POST['distancia'], @str_replace(",", ".", str_replace(".", "", $_POST['frete'])), $_POST['numero']
        )) {

            for ($i = 1; $i <= 7; $i++) {
                if (isset($_POST['d' . $i])) {

                    @$this->o_fornecedor->storeParametrosPeriodo(@$_POST['email'], $i, @$_POST['she' . $i] . ":" . @$_POST['sme' . $i], @$_POST['shs' . $i] . ":" . @$_POST['sms' . $i], 'A');
                } else {

                    @$this->o_fornecedor->storeParametrosPeriodo(@$_POST['email'], $i, '00:00', '00:00', 'F');
                }
            }

            for ($i = 1; $i <= 6; $i++) {
                if (isset($_POST['p' . $i])) {

                    $this->o_fornecedor->storeParametrosPagamento(@$_POST['email'], $i);
                } else {
                    
                }
            }

            header("location: index.php?message=ok");
        } else {

            header("location: register.php?message=fail");
        }
    }

    function loginAction() {

        $response = array("error" => FALSE);

        if (isset($_POST['email']) && isset($_POST['senha'])) {

            // receiving the post params
            $email = $_POST['email'];
            $password = $_POST['senha'];

            // get the user by email and password
            $user = $this->o_fornecedor->getUserByEmailAndPassword($email, $password);

            if ($user != false) {
                // use is found
                $usertoken = $this->o_fornecedor->storeToken($email);

                setcookie("token", $usertoken['token'], time() + (60 * 60 * 24 * 365), "/");
                setcookie("email", $usertoken['email'], time() + (60 * 60 * 24 * 365), "/");
                setcookie("id_fornecedor", $user['id_fornecedor'], time() + (60 * 60 * 24 * 365), "/");
                // echo $_COOKIE["token2"];     
                header("Location: home/pages/");
            } else {
                // user is not found with the credentials
                header("location: index.php?message=fail");
            }
        } else {
            // required post params is missing
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters email or password is missing!";
            echo json_encode($response);
        }
    }

}

