<?php

/**
 * Controlador que deverá ser chamado quando não for
 * especificado nenhum outro.
 */
class IndexController {

    /**
     * Ação que deverá ser executada quando
     * nenhuma outra for especificada, do mesmo jeito que o
     * arquivo index.php é executado quando nenhum é referenciado.
     */
    public function indexAction() {
        //Redirecionando para a pagina default
        header('Location: ?controle=Fornecedor&acao=categoria');
    }
}