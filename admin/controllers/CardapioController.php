<?php

require_once 'models/CardapioModel.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CardapioController {

    protected $o_cardapio;

    /**
     * Construtor padrão responsável por novas instâncias dessa classe.
     *
     * FornecedorController constructor.
     */
    public function __construct() {

        $this->o_cardapio = new CardapioModel();
    }





public function storeAction(){
    @$id_fornecedor = $_COOKIE['id_fornecedor'];
    $categoria = @$_POST['categoria'];
    $valor = @str_replace(",", ".", str_replace(".", "", $_POST['valor']));
    $descricao = @$_POST['descricao'];
    $nome = @$_POST['nome'];
    $tempo = @$_POST['horas'] + @$_POST['minutos'];
    $id_cardapio = @$_POST['id_cardapio'];

    if(empty($id_cardapio)){
    if (empty($descricao) or empty($valor) or empty($categoria) or empty($nome)) {
        echo "{\"erro\": \"Existem campos não informados\"}";
    } else {
        //    return false;



     
       
        $cardapio = $this->o_cardapio->storeCardapio($nome, $id_fornecedor, $descricao, $categoria, $valor, $tempo);
        if ($cardapio) {
            echo "{\"erro\": \"false\"}";
        } else {
            //  return false;
            echo "{\"erro\": \"true\"}";
        }
    }
    }

    else{
       
        $cardapio = $this->o_cardapio->updateCardapio($nome, $tempo, $descricao, $valor, $categoria, $id_fornecedor, $id_cardapio);
        if ($cardapio) {
            echo "{\"erro\": \"false\"}";
        } else {
            //  return false;
             echo "{\"erro\": \"true\"}";
        }
        
    }
}

public function getAllAction() {
        @$id_fornecedor = $_COOKIE['id_fornecedor'];

    
    @$cardapio = $this->o_cardapio->getAllCardapio($id_fornecedor);
    
    echo json_encode($cardapio);
    
}

public function getAction() {
      @$id_fornecedor = $_COOKIE['id_fornecedor'];
      @$id_cardapio = $_GET['id_cardapio'];
    $cardapio = $this->o_cardapio->getCardapio($id_cardapio,$id_fornecedor);
    echo json_encode($cardapio);
    
}

public function deleteAction() {
     @$id_fornecedor = $_COOKIE['id_fornecedor'];

     @$id_cardapio = $_REQUEST['id_cardapio'];
     
   $this->o_cardapio->deleteCardapio($id_cardapio, $id_fornecedor);
    
    echo "{\"erro\": \"delete\"}";
    

}

}