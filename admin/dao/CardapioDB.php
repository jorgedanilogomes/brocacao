<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ComplementoDB
 *
 * @author karlos
 */
class CardapioDB {

    //put your code here

  
    private $conn;
    private $pdo;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
       // $db = new Db_Connect();
      //  $this->conn = $db->connect();
      //$this->pdo = new PDO('mysql:host=localhost;dbname=fazco329_brc', 'fazco329_brc', 'brc123');
        $this->pdo = new PDO('mysql:host=localhost;dbname=brc', 'root', '');
    }

    // destructor
    function __destruct() {
        
    }

    public function storeCardapio($nome, $fornecedor, $descricao, $categoria, $valor, $tempo) {

        if (@$valor > 0.0) {

            $stmt = $this->pdo->prepare("SELECT * from cardapio WHERE nome = :nome and fornecedor = :fornecedor and status = 'A' ");
           
            $dados = array( ":nome" => $nome,":fornecedor"=>$fornecedor);
            $stmt->execute($dados);
            

            if ($stmt->rowCount() > 0) {
                // se existir retorna com erro
                $stmt = null;
                echo "{\"erro\": \"Já existe um item com esta descrição! \"}";
            } else {

                $stmt = $this->pdo->prepare("INSERT INTO cardapio (nome,tempo_preparo,descricao,valor,categoria,fornecedor,status) VALUES ( :nome, :tempo_preparo, :descricao, :valor ,:categoria ,:fornecedor,'A'  )");
          
                $dados = array( ":nome" => $nome,":fornecedor"=>$fornecedor,":descricao"=>$descricao,":valor"=>$valor,":categoria"=>$categoria,":tempo_preparo"=>$tempo);
                $result = $stmt->execute($dados);


                $stmt=null;
                if ($result) {

                    $stmt = $this->pdo->prepare("SELECT * from cardapio WHERE descricao = :descricao and fornecedor = :fornecedor and status='A'");
                    $dados = array( ":descricao" => $descricao,":fornecedor"=>$fornecedor);
                    $stmt->execute($dados);
                   
                    $stmt = null;

                    return true;
                } else {
                    // return $stmt->error;
                    
                     echo "{\"erro\": \" erro !\"}";
                     $stmt->close();
                }
            }
        } else {
            echo "{\"erro\": \"O valor tem que ser maior que 0 !\"}";
        }
    }

    public function getAllCardapio($fornecedor) {

        $stmt = $this->pdo->prepare("SELECT a.id_cardapio as id_cardapio,"
                . " a.descricao as descricao,"
                . " a.tempo_preparo as tempo,"
                . " a.nome as nome,"
                . " b.descricao as categoria,"
                 . " a.tempo_preparo div 60 as horas,"
                . " a.tempo_preparo % 60 as minutos,"
                . " format(a.valor,2,'de_DE') as valor FROM cardapio a, categoria b where a.categoria=b.id_categoria and fornecedor = :fornecedor and a.status='A'");
       
        $dados = array( ":fornecedor"=>$fornecedor);

        if ($stmt->execute($dados)) {

          

            $cardapios = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
            $stmt=null;
            return $cardapios;
        } else {
            return false;
        }
    }

    public function getCardapio($id,$fornecedor) {

             $stmt = $this->pdo->prepare("SELECT a.id_cardapio as id_cardapio,"
                . " a.descricao as descricao,"
                . " a.tempo_preparo as tempo,"
                . " a.nome as nome,"
                . " b.descricao as categoria,"
                 . " a.tempo_preparo div 60 as horas,"
                . " a.tempo_preparo % 60 as minutos,"
                . " format(a.valor,2,'de_DE') as valor FROM cardapio a, categoria b where a.categoria=b.id_categoria and fornecedor = :fornecedor and id_cardapio = :id_cardapio");
        $dados = array( ":fornecedor"=>$fornecedor,":id_cardapio"=>$id);

        if ($stmt->execute($dados)) {

          

            $cardapios = $stmt->fetchAll(PDO::FETCH_ASSOC);
         
            $stmt = null;
            return $cardapios;
        } else {
            return false;
        }
    }

    public function deleteCardapio($id,$fornecedor) {


        $stmt = $this->pdo->prepare("update cardapio set status='D' WHERE id_cardapio = :id_cardapio and fornecedor = :fornecedor ");
        $dados = array( ":fornecedor"=>$fornecedor,":id_cardapio"=>$id);
        $stmt->execute($dados);
        

        if ($stmt->rowCount() > 0) {
            // se existir retorna com erro
            $stmt = null;
            return true;
        } else {
            return false;
        }
    }

    public function getAllCategoria() {



        $stmt = $this->pdo->prepare("SELECT * FROM categoria where status='A' ");





        if ($stmt->execute()) {

           

            $categorias = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt = null;
            return $categorias;
        } else {
            return false;
        }
    }
    
     public function updateCardapio($nome, $tempo_preparo, $descricao, $valor,$categoria,$fornecedor,$id_cardapio) {


        $stmt = $this->pdo->prepare("update cardapio set nome = :nome,tempo_preparo = :tempo_preparo,descricao = :descricao,valor = :valor,categoria = :categoria where fornecedor = :fornecedor and id_cardapio = :id_cardapio");
              $dados = array( ":nome" => $nome,":fornecedor"=>$fornecedor,":descricao"=>$descricao,":valor"=>$valor,":categoria"=>$categoria,":tempo_preparo"=>$tempo_preparo,":id_cardapio"=>$id_cardapio);
       
        if($stmt->execute($dados)){
            return true;
        }
        else{
             echo "{\"erro\": \"$stmt->error !\"}";
        }
       

       
    }

}
