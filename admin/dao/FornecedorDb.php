<?php
 
class FornecedorDb {
 
    private $conn;
    private $pdo;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
       // $db = new Db_Connect();
      //  $this->conn = $db->connect();
        //$this->pdo = new PDO('mysql:host=localhost;dbname=fazco329_brc', 'fazco329_brc', 'brc123');
         $this->pdo = new PDO('mysql:host=localhost;dbname=brocadao', 'root', '');
    }

    // destructor
    function __destruct() {
         
    }
 
  
    /**
     * Get user by email and password
     */

  public function getUserByEmailAndPassword($email, $password) {
      $salt="";
      $stmt = $this->pdo->prepare("SELECT * FROM fornecedores WHERE email = :email and tipo= 'F' and status='A'");
      
      $dados = array( ":email" => $email);
      
                   
      $stmt->execute($dados);
          if ($stmt->rowCount() > 0) {
            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt=null;
           // var_dump($user);
            $salt = $user[0]['salt'];
            
            
        } else {
            return false;
        }
      $pass = $this->checkhashSSHA($salt, $password);
      $stmt = $this->pdo->prepare("SELECT * FROM fornecedores WHERE email = :email and senha = :password  and tipo= 'F' and status='A'");
        
         $dados = array( ":email" => $email,"password"=>$pass);
 
        if ($stmt->execute($dados)) {
            $user =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt=null;
            return $user[0];
        } else {
            return false;
        }
        
    }
 
    /**
     * Check user is existed or not
     */
    public function isUserExisted($email) {
        $stmt = $this->pdo->prepare("SELECT email from fornecedores WHERE email = :email");
 
          $dados = array( ":email" => $email);
 
        $stmt->execute($dados);
 

 
        if ($stmt->rowCount() > 0) {
            // user existed
            $stmt=null;
            return true;
        } else {
            // user not existed
            $stmt=null;
            return false;
        }
    }
 
    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {
 
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
 
        return $hash;
    }
    
    
     public function storeFornecedor($nome,$frase, $email, $password, $rua,$bairro,$cep,$complemento,$cidade,$estado,$latitude,$longitude,$tipo,$cgc,$categoria,$valor_minimo,$distancia,$valor_frete,$numero) {
//var_dump($fornecedorTest->storeFornecedor("Xiquinhos Burgs2","frase", "xiquinho1712@gmail.com", "fornecedorTest", "hm06", "Maria Dilce", "74584225", "Qd 10 Lt 26", "Goiânia", "GO", "740000.000", "843984.29894", "F","733522","1.5","1","1.5","1"));

         
        $stmt = $this->pdo->prepare("SELECT email from fornecedores WHERE email = :email and tipo='F'");
        $dados = array( ":email" => $email);
        $stmt->execute($dados);
      
 
        if ($stmt->rowCount() > 0) {
            // se existir retorna com erro
            $stmt=null;
            return false;
        } else {
        
         
         
        $uuid = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
 
        $stmt = $this->pdo->prepare("INSERT INTO fornecedores(nome,frase,senha, email, bairro,rua,cep,complemento,cidade,estado,latitude,longitude,salt,data_cadastro,tipo,status,cgc,categoria,distancia_entrega,valor_minimo,valor_frete,numero) VALUES( :nome, :frase, :senha, :email, :bairro, :rua, :cep, :complemento, :cidade, :estado, :latitude, :longitude,:salt, '2015-05-01 15:00:00',:tipo,'A',:cgc,:categoria,:distancia_entrega,:valor_minimo,:valor_frete,:numero)");
      
      // $stmt = $this->c.onn->prepare("INSERT INTO fornecedores(id_usuario,nome,senha, email, bairro,rua,cep,complemento,cidade,estado,latitude,longitude,salt,data_cadastro,tipo) VALUES(252, 'fornecedorTest fornecedorTest x', 'fdafds','fornecedorTest@testefdf','Maria dilce', 'hm6', '74584225', 'qd 10 lt 26', 'goiania', 'GO', 34123432, 4124323,'312qqwe', '2015-05-01 15:00:00','F')");
  $dados = array(":nome"=>$nome,":frase"=>$frase,":senha"=>$encrypted_password,":email" => $email,":bairro"=>$bairro,":rua"=>$rua,":cep"=>$cep,":complemento"=>$complemento,":cidade"=>$cidade,":estado"=>$estado,":latitude"=>$latitude,":longitude"=>$longitude,":salt"=>$salt,":tipo"=>$tipo,":cgc"=>$cgc,":categoria"=>$categoria,":distancia_entrega"=>$distancia,":valor_minimo"=>$valor_minimo,":valor_frete"=>$valor_frete,":numero"=>$numero);
       $result =  $stmt->execute($dados);
      // $result = $stmt->error;
       // return true;
        
       
          $stmt=null;
        if ($result) {
         
           
 
            return true;
        } else {
            //return $stmt->error;
        
            return false;
        }
       
        }
     }
     
        public function storeToken($email) {
      
            // se existir retorna com erro
        mt_srand (time());
        $token = mt_rand(1000000,999999999).rand().substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);         
        $stmt = $this->pdo->prepare("SELECT * from token_api WHERE email = :email");
        $dados = array(":email"=>$email);
        $stmt->execute($dados);
        
 
        if ($stmt->rowCount() > 0) {

            
        $stmt = $this->pdo->prepare("UPDATE token_api set token = :token where email = :email");
        $dados = array(":email"=>$email,":token"=>$token);
        $stmt->execute($dados);
        $response = array();
        $response['email'] = $email;
        $response['token'] = $token;
        return $response;
        } else {
        
        $stmt = $this->pdo->prepare("INSERT INTO token_api(email,token) VALUES( :email, :token)");
         $dados = array(":email"=>$email,":token"=>$token);
         $stmt->execute($dados);
        $response = array();
        $response['email'] = $email;
        $response['token'] = $token;
        return $response;
       
        }
     }
    
     
     
      public function getAllCategoria() {
     

    
      $stmt = $this->pdo->prepare("SELECT * FROM categoria_fornecedor");
        
      
      
      
      
        if ($stmt->execute()) {
            
            $categorias = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
           
            return $categorias;
        } else {
            return false;
        }
        
    }
    
    
       public function checkToken($email,$token) {
        $stmt = $this->pdo->prepare("SELECT * from token_api WHERE email = :email and token = :token");
  $dados = array(":email"=>$email,":token"=>$token);
        
 
        $stmt->execute($dados);
 
     
        if ($stmt->rowCount() > 0) {
            // user existed
            $stmt=null;
            return true;
        } else {
            // user not existed
            $stmt=null;
            return false;
        }
    }
    
    public function storeParametrosPeriodo($fornecedor, $dia, $hora_inicio, $hora_fim,$status) {

  

            $stmt = $this->pdo->prepare("SELECT * from periodo_funcionamento WHERE email_fornecedor = :fornecedor and dia = :dia ");
              $dados = array(":fornecedor"=>$fornecedor,":dia"=>$dia);

            $stmt->execute($dados);
            

            if ($stmt->rowCount() > 0) {
                // se existir retorna com erro
                $stmt=null;
                echo "{\"erro\": \"Parametro já cadastrado \"}";
            } else {

                $stmt = $this->pdo->prepare("INSERT INTO periodo_funcionamento (email_fornecedor,dia,hora_abertura,hora_fechamento,status) VALUES ( :fornecedor, :dia, :hora_abertura, :hora_fechamento,:status )");
                 $dados = array(":fornecedor"=>$fornecedor,":dia"=>$dia,":hora_abertura"=>$hora_inicio,":hora_fechamento"=>$hora_fim,":status"=>$status);

                $result = $stmt->execute($dados);


                $stmt=null;
                if ($result) {

                    $stmt = $this->pdo->prepare("SELECT * from periodo_funcionamento WHERE email_fornecedor = :fornecedor and dia = :dia ");
                    $dados = array(":fornecedor"=>$fornecedor,":dia"=>$dia);
                    $stmt->execute($dados);
                    
                    $stmt=null;

                    return true;
                } else {
                    // return $stmt->error;
                    
                     echo "{\"erro\": \"$stmt->error !\"}";
                     $stmt=null;
                }
            }
      
    }
    
    public function storeParametrosRegras($fornecedor, $valor, $distancia) {

  

            $stmt = $this->pdo->prepare("SELECT * from parametros WHERE id_fornecedor = :fornecedor  ");
            $dados = array(":fornecedor"=>$fornecedor);
            $stmt->execute();
            $stmt->store_result();

            if ($stmt->rowCount() > 0) {
                // se existir retorna com erro
                $stmt=null;
                echo "{\"erro\": \"Parametro já cadastrado \"}";
            } else {

                $stmt = $this->pdo->prepare("INSERT INTO parametros (id_fornecedor,valor_minimo,distancia_entrega) VALUES ( :fornecedor, :valor, :distancia)");
               $dados = array(":fornecedor"=>$fornecedor,":valor"=>$valor,":distancia"=>$distancia);

                $result = $stmt->execute($dados);


                $stmt=null;
                if ($result) {

                    $stmt = $this->pdo->prepare("SELECT * from parametros WHERE id_fornecedor = :fornecedor  ");
                    $dados = array(":fornecedor"=>$fornecedor);
                    $stmt->execute($dados);
                    $stmt=null;

                    return true;
                } else {
                    // return $stmt->error;
                    
                     echo "{\"erro\": \"$stmt->error !\"}";
                     $stmt=null;
                }
            }
      
    }
    
      
       public function getFornecedoresbyDistance($latitude,$longitude) {


        $dia= jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 0 )+1;
        $stmt = $this->pdo->prepare("select * from (SELECT id_fornecedor,nome,frase, email,bairro,rua,cep,complemento,cidade,estado,latitude,longitude,c.descricao as categoria,img,status,distancia_entrega,( 6371 * acos( cos( radians(:latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians( latitude ) ) ) ) AS distance FROM fornecedores f, categoria_fornecedor c where f.categoria=c.id_categoria_fornecedor ) as fornecedores, (select * from periodo_funcionamento pf where  dia=".$dia." and status='A' ) as func where func.email_fornecedor=fornecedores.email and distance < distancia_entrega and fornecedores.status='A'  ORDER BY rand()");

        
        
              
        $dados = array(":latitude"=>$latitude,":longitude"=>$longitude);

        if ($stmt->execute($dados)) {

           

            $fornecedores = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt=null;
               $i=0;
          foreach ($fornecedores as $value) {
                 $consulta = $this->pdo->prepare("select * from forma_pagamento a, pagamento_fornecedor b where email_fornecedor='".$value['email']."' and a.id_pagamento=b.id_pagamento");
    
                 $consulta->execute();
                   $result2 = $consulta->fetchAll(PDO::FETCH_ASSOC);
                 
               $res[$i] = array("fornecedor"=>$value,"pagamento"=>$result2);
             // var_dump($result2);
                   $i++; 
            }
            
            
            
       
            return $res;
        } else {
            return false;
        }
    }
    
    
        
    public function storeParametrosPagamento($fornecedor, $id_forma_pagamento) {

  

            $stmt = $this->pdo->prepare("SELECT * from pagamento_fornecedor WHERE email_fornecedor = :fornecedor and id_pagamento = :id_pagamento ");
            $dados = array(":fornecedor"=>$fornecedor,":id_pagamento"=>$id_forma_pagamento);
            $stmt->execute($dados);
        

            if ($stmt->rowCount() > 0) {
                // se existir retorna com erro
                $stmt=null;
                echo "{\"erro\": \"Parametro já cadastrado \"}";
            } else {

                $stmt = $this->pdo->prepare("INSERT INTO pagamento_fornecedor (email_fornecedor,id_pagamento) VALUES ( :email_fornecedor, :id_pagamento)");
                
$dados = array(":email_fornecedor"=>$fornecedor,":id_pagamento"=>$id_forma_pagamento);
                $result = $stmt->execute($dados);


                $stmt=null;
                if ($result) {

                 

                    return true;
                } else {
                    // return $stmt->error;
                    
                     echo "{\"erro\": \"$stmt->error !\"}";
                     $stmt=null;
                }
            }
      
    }
    
    function getFornecedor($latitude, $longitude,$id){
        $dia= jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 0 )+1;
         $consulta = $this->pdo->prepare("select * from (SELECT id_fornecedor,nome,frase, email,bairro,rua,cep,complemento,cidade,estado,valor_frete,valor_minimo,latitude,longitude,c.descricao as categoria,img,status,distancia_entrega,( 6371 * acos( cos( radians(:latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians( latitude ) ) ) ) AS distance FROM fornecedores f, categoria_fornecedor c where f.categoria=c.id_categoria_fornecedor ) as fornecedores, (select * from periodo_funcionamento pf where  dia=".$dia." and status='A' ) as func where func.email_fornecedor=fornecedores.email and distance < distancia_entrega and fornecedores.status='A' and fornecedores.id_fornecedor=:id  ORDER BY rand()");
     $dados = array(":latitude" => $latitude, ":longitude" => $longitude, ":id" => $id);
                 $consulta->execute($dados);
                   $result = $consulta->fetchAll(PDO::FETCH_ASSOC);
       
                        $i=0;
                        $res = array();
          foreach ($result as $value) {
                 $consulta = $this->pdo->prepare("select * from forma_pagamento a, pagamento_fornecedor b where email_fornecedor='".$value['email']."' and a.id_pagamento=b.id_pagamento ");
    
                 $consulta->execute();
                   $result2 = $consulta->fetchAll(PDO::FETCH_ASSOC);
                 
               $res[$i] = array("fornecedor"=>$value,"pagamento"=>$result2);
             // var_dump($result2);
                   $i++; 
            }
            
            
                   
                   
                   return $res;
                   
    }
    
    
    function getCardapioFornecedor($id){

         $consulta = $this->pdo->prepare("select id_cardapio,c.nome as nome, c.descricao as descricao, tempo_preparo,valor, categoria,fornecedor, id_categoria,img from cardapio c , categoria g  where  c.categoria=g.id_categoria and c.fornecedor = :id and c.status='A'");
     $dados = array( ":id" => $id);
                 $consulta->execute($dados);
                   $result = $consulta->fetchAll(PDO::FETCH_ASSOC);
    
    return $result;
}
    function getCardapiobyId($id_cardapio,$id_fornecedor){

         $consulta = $this->pdo->prepare("select id_cardapio,c.nome as nome, c.descricao as descricao, tempo_preparo,valor, categoria,fornecedor, id_categoria,img from cardapio c , categoria g  where  c.categoria=g.id_categoria and c.fornecedor = :id_fornecedor and c.id_cardapio = :id_cardapio and c.status='A'");
     $dados = array( ":id_cardapio" => $id_cardapio,":id_fornecedor" => $id_fornecedor);
                 $consulta->execute($dados);
                   $result = $consulta->fetchAll(PDO::FETCH_ASSOC);
    $ress = array();
                   $i=0;
        foreach ($result as $value) {
                 $consulta = $this->pdo->prepare("select * from complemento where id_fornecedor = :id_fornecedor and status = 'A' and categoria = ".$value['categoria'] );
                  $dados = array(":id_fornecedor"=>$id_fornecedor);
                 $consulta->execute($dados);
                   $result2 = $consulta->fetchAll(PDO::FETCH_ASSOC);
                 
               $res[$i] = array("cardapio"=>$value,"complemento"=>$result2);
             // var_dump($result2);
                   $i++; 
            }
            
            
                   
                   //return
                   return $res;
}

 
}