<?php
 
class DB_Functions {
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }
 
    // destructor
    function __destruct() {
         
    }
 
    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($name, $email, $password) {
        $uuid = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
 
        $stmt = $this->conn->prepare("INSERT INTO users(unique_id, name, email, encrypted_password, salt, created_at) VALUES(?, ?, ?, ?, ?, NOW())");
        $stmt->bind_param("sssss", $uuid, $name, $email, $encrypted_password, $salt);
        $result = $stmt->execute();
        $stmt->close();
 
        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
 
            return $user;
        } else {
            return false;
        }
    }
 
    /**
     * Get user by email and password
     */
    public function getUserByEmailAndPassword($email, $password) {
 
        $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
 
        $stmt->bind_param("s", $email);
 
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
 
    /**
     * Check user is existed or not
     */
    public function isUserExisted($email) {
        $stmt = $this->conn->prepare("SELECT email from users WHERE email = ?");
 
        $stmt->bind_param("s", $email);
 
        $stmt->execute();
 
        $stmt->store_result();
 
        if ($stmt->num_rows > 0) {
            // user existed
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
 
    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {
 
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
 
        return $hash;
    }
    
    
     public function storeFornecedor($nome, $email, $password, $rua,$bairro,$cep,$complemento,$cidade,$estado,$latitude,$longitude,$tipo) {
      
         
        $stmt = $this->conn->prepare("SELECT email from usuarios WHERE email = ? ");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows > 0) {
            // se existir retorna com erro
            $stmt->close();
            return false;
        } else {
        
         
         
        $uuid = uniqid('F', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
 
        $stmt = $this->conn->prepare("INSERT INTO usuarios(nome,senha, email, bairro,rua,cep,complemento,cidade,estado,latitude,longitude,salt,data_cadastro,tipo) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '2015-05-01 15:00:00',?)");
        $stmt->bind_param("sssssssssssss", $nome, $password,$email, $bairro,$rua,$cep,$complemento,$cidade,$estado,$latitude,$longitude,$password,$tipo);
      // $stmt = $this->conn->prepare("INSERT INTO usuarios(id_usuario,nome,senha, email, bairro,rua,cep,complemento,cidade,estado,latitude,longitude,salt,data_cadastro,tipo) VALUES(252, 'fornecedorTest fornecedorTest x', 'fdafds','fornecedorTest@testefdf','Maria dilce', 'hm6', '74584225', 'qd 10 lt 26', 'goiania', 'GO', 34123432, 4124323,'312qqwe', '2015-05-01 15:00:00','F')");
  
       $result =  $stmt->execute();
       //$result = $stmt->error;
       // return $result;
        
       
       
        if ($result) {
            $stmt->close();
            $stmt = $this->conn->prepare("SELECT * FROM usuarios WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result();
            $stmt->close();
 
            return true;
        } else {
            return false;
        }
       
        }
     } 
}
 
?>