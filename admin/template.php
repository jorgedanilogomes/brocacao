<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Registration Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <style>
            .linha-registro div{
                padding: 5px 5px !important;

            }
        </style>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
   <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyD8ycY-1IkXkwWmPmUcYVPx-r2eow-pkyA&libraries=places'></script>
        <script src="assets/js/locationpicker.jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
        <link rel="stylesheet" href="http://jqueryvalidation.org/files/demo/site-demos.css">
        <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
        <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal.css" />
        <link href="assets/css/toastr.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/css/jslider.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.blue.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.plastic.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.round.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jslider.round.plastic.css" type="text/css">
        <!-- end -->



        <script type="text/javascript" src="assets/js/angular.js"></script>
        
        <!-- bin/jquery.slider.min.js -->
        <script type="text/javascript" src="assets/js/jshashtable-2.1_src.js"></script>
        
        <script type="text/javascript" src="assets/js/jquery.numberformatter-1.2.3.js"></script>
        <script type="text/javascript" src="assets/js/tmpl.js"></script>
        <script type="text/javascript" src="assets/js/jquery.dependClass-0.1.js"></script>
        <script type="text/javascript" src="assets/js/draggable-0.1.js"></script>
        <script type="text/javascript" src="assets/js/jquery.slider.js"></script>
        <script src="assets/js/toastr.min.js"></script>
        <style type="text/css" media="screen">

            .layout { padding: 50px; font-family: Georgia, serif; }
            .layout-slider { margin-bottom:  60px; margin-top: 30px; width: 100%;  }
            .layout-slider-settings {  padding-bottom: 10px; }
            .layout-slider-settings pre { font-family: Courier;  }
            span {font-size: 15px;}
        </style>

        <?php
        if (@$_GET["message"] == "fail") {
            echo "<script>


    $(document).ready(function() {

toastr.options = {\"positionClass\": \"toast-top-full-width\"}      
toastr.info('Falha ao registrar usuário!')
});</script>";
        }
        ?>

    </head>


