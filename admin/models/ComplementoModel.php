<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ComplementoDB
 *
 * @author karlos
 */
class ComplementoModel extends PersistModelAbstract {

   
 
    // constructor
    function __construct() {
          parent::__construct();
    }

    // destructor
    function __destruct() {
        
    }

    public function storeComplemento($fornecedor, $descricao, $categoria, $valor) {

        if (@$valor > 0.0) {

            $stmt = $this->o_db->prepare("SELECT * from complemento WHERE descricao = :descricao and id_fornecedor = :id_fornecedor and categoria = :categoria and status='A'");
            $dados = array( ":descricao" => $descricao,":id_fornecedor"=>$fornecedor,":categoria"=>$categoria);
            $stmt->execute($dados);
            

            if ($stmt->rowCount() > 0) {
                // se existir retorna com erro
                $stmt = null;
                echo "{\"erro\": \"Já existe um item com esta descrição! \"}";
            } else {

                $stmt = $this->o_db->prepare("INSERT INTO complemento (descricao,id_fornecedor,categoria,valor,status) VALUES ( :descricao, :id_fornecedor, :categoria, :valor , 'A')");
                   $dados = array( ":descricao" => $descricao,":id_fornecedor"=>$fornecedor,":categoria"=>$categoria,":valor"=>$valor);

                $result = $stmt->execute($dados);


                $stmt = null;
                if ($result) {

                    $stmt = $this->o_db->prepare("SELECT * from complemento WHERE descricao = :descricao and id_fornecedor = :id_fornecedor and status='A'");
                    $dados = array( ":descricao" => $descricao,":id_fornecedor"=>$fornecedor);
                    $stmt->execute($dados);
                    
                    $stmt = null;

                    return true;
                } else {
                    // return $stmt->error;
                    
                     echo "{\"erro\": \"error ao gravar dados !\"}";
                     $stmt->close();
                }
            }
        } else {
            echo "{\"erro\": \"O valor tem que ser maior que 0 !\"}";
        }
    }

    public function getAllComplemento($fornecedor) {

        $stmt = $this->o_db->prepare("SELECT a.id_complemento as id_complemento,"
                . " a.descricao as descricao,"
                . " b.descricao as categoria,"
                . " format(a.valor,2,'de_DE') as valor FROM complemento a, categoria b where a.categoria=b.id_categoria and id_fornecedor = :id_fornecedor and a.status='A'");
            $dados = array(":id_fornecedor"=>$fornecedor);

        if ($stmt->execute($dados)) {

            

            $complementos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $stmt = null;
            return $complementos;
        } else {
            return false;
        }
    }

    public function getComplemento($id,$fornecedor) {

             $stmt = $this->o_db->prepare("SELECT a.id_complemento as id_complemento,"
                . " a.descricao as descricao,"
                . " b.descricao as categoria,"
                . " format(a.valor,2,'de_DE') as valor, b.id_categoria as id_categoria FROM complemento a, categoria b where a.categoria=b.id_categoria and id_fornecedor = :id_fornecedor and id_complemento = :id_complemento ");
         $dados = array(":id_fornecedor"=>$fornecedor,":id_complemento"=>$id);

        if ($stmt->execute($dados)) {

            

            $complementos = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $stmt = null;
            return $complementos;
        } else {
            return false;
        }
    }

    public function deleteComplemento($id,$fornecedor) {


        $stmt = $this->o_db->prepare("update complemento set status='D' WHERE id_complemento = :id_complemento and id_fornecedor = :id_fornecedor ");
        $dados = array(":id_fornecedor"=>$fornecedor,":id_complemento"=>$id);
        $stmt->execute($dados);
        

        if ($stmt->rowCount() > 0) {
            // se existir retorna com erro
            $stmt = null;
            return true;
        } else {
            return false;
        }
    }

    public function getAllCategoria() {



        $stmt = $this->o_db->prepare("SELECT * FROM categoria where status='A'");





        if ($stmt->execute()) {


            $categorias = $stmt->fetchAll(PDO::FETCH_ASSOC);
          
            $stmt = null;
            return $categorias;
        } else {
            return false;
        }
    }
    
     public function updateComplemento($fornecedor, $descricao, $categoria, $valor,$id_complemento) {


        $stmt = $this->o_db->prepare("update complemento set descricao= :descricao, categoria = :categoria , valor = :valor where id_fornecedor = :id_fornecedor and id_complemento = :id_complemento");
        $dados = array( ":descricao" => $descricao,":id_fornecedor"=>$fornecedor,":categoria"=>$categoria,":id_complemento"=>$id_complemento,":valor"=>$valor);
       
        if($stmt->execute($dados)){
            return true;
        }
        else{
             echo "{\"erro\": \"Erro !\"}";
        }
       

       
    }

}
