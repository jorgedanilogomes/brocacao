<?php

/**
 * Responsável por gerenciar e persistir os dados do Fornecedor
 *
 * Class FornecedorModel
 */
class FornecedorModel extends PersistModelAbstract {

    private $nome;
    private $frase;
    private $senha;
    private $email;
    private $bairro;
    private $rua;
    private $cep;
    private $complemento;
    private $cidade;
    private $estado;
    private $latitude;
    private $longitude;
    private $salt;
    private $data_cadastro;
    private $tipo;
    private $status;
    private $cgc;
    private $categoria;
    private $distancia_entrega;
    private $valor_minimo;
    private $valor_frete;
    private $numero;


    /**
     * Construtor responsável por novas instâncias dessa classe.
     */
    function __construct() {
        parent::__construct();
    }


    /**
     * Get user by email and password
     */
    public function getUserByEmailAndPassword($email, $password) {
        $salt = "";

        $stmt = $this->o_db->prepare("SELECT * FROM fornecedores WHERE email = :email and tipo= 'F' and status='A'");

        $dados = array(":email" => $email);

        $stmt->execute($dados);

        if ($stmt->rowCount() > 0) {

            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            $salt = $user[0]['salt'];

        } else {

            return false;
        }

        $pass = $this->checkhashSSHA($salt, $password);

        $stmt = $this->o_db->prepare("SELECT * FROM fornecedores WHERE email = :email and senha = :password  and tipo= 'F' and status='A'");

        $dados = array(":email" => $email, "password" => $pass);

        if ($stmt->execute($dados)) {

            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $user[0];
        } else {

            return false;
        }

    }

    /**
     * Check user is existed or not
     */
    public function isUserExisted($email) {

        $stmt = $this->o_db->prepare("SELECT email from fornecedores WHERE email = :email");

        $dados = array(":email" => $email);

        $stmt->execute($dados);

        if ($stmt->rowCount() > 0) {

            $stmt = null;

            return true;
        } else {

            $stmt = null;

            return false;
        }
    }

    /**
     * @param $password
     * @return array
     */
    public function hashSSHA($password)  {

        $salt = sha1(rand());

        $salt = substr($salt, 0, 10);

        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);

        $hash = array("salt" => $salt, "encrypted" => $encrypted);

        return $hash;
    }


    /**
     * @param $salt
     * @param $password
     * @return string
     */
    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }


    public function storeFornecedor($nome, $frase, $email, $password, $rua, $bairro, $cep, $complemento, $cidade, $estado, $latitude, $longitude, $tipo, $cgc, $categoria, $valor_minimo, $distancia, $valor_frete, $numero) {

        //var_dump($fornecedorTest->storeFornecedor("Xiquinhos Burgs2","frase", "xiquinho1712@gmail.com", "fornecedorTest", "hm06", "Maria Dilce", "74584225", "Qd 10 Lt 26", "Goiânia", "GO", "740000.000", "843984.29894", "F","733522","1.5","1","1.5","1"));

        $stmt = $this->o_db->prepare("SELECT email from fornecedores WHERE email = :email and tipo='F'");

        $dados = array(":email" => $email);

        $stmt->execute($dados);

        if ($stmt->rowCount() > 0) {

            $stmt = null;

            return false;

        } else {

            $uuid = uniqid('', true);

            $hash = $this->hashSSHA($password);

            $encrypted_password = $hash["encrypted"]; // encrypted password

            $salt = $hash["salt"]; // salt

            $stmt = $this->o_db->prepare("INSERT INTO fornecedores(nome,frase,senha, email, bairro,rua,cep,complemento,cidade,estado,latitude,longitude,salt,data_cadastro,tipo,status,cgc,categoria,distancia_entrega,valor_minimo,valor_frete,numero) VALUES( :nome, :frase, :senha, :email, :bairro, :rua, :cep, :complemento, :cidade, :estado, :latitude, :longitude,:salt, '2015-05-01 15:00:00',:tipo,'A',:cgc,:categoria,:distancia_entrega,:valor_minimo,:valor_frete,:numero)");

            // $stmt = $this->c.onn->prepare("INSERT INTO fornecedores(id_usuario,nome,senha, email, bairro,rua,cep,complemento,cidade,estado,latitude,longitude,salt,data_cadastro,tipo) VALUES(252, 'fornecedorTest fornecedorTest x', 'fdafds','fornecedorTest@testefdf','Maria dilce', 'hm6', '74584225', 'qd 10 lt 26', 'goiania', 'GO', 34123432, 4124323,'312qqwe', '2015-05-01 15:00:00','F')");

            $dados = array(":nome" => $nome, ":frase" => $frase, ":senha" => $encrypted_password, ":email" => $email, ":bairro" => $bairro, ":rua" => $rua, ":cep" => $cep, ":complemento" => $complemento, ":cidade" => $cidade, ":estado" => $estado, ":latitude" => $latitude, ":longitude" => $longitude, ":salt" => $salt, ":tipo" => $tipo, ":cgc" => $cgc, ":categoria" => $categoria, ":distancia_entrega" => $distancia, ":valor_minimo" => $valor_minimo, ":valor_frete" => $valor_frete, ":numero" => $numero);

            $result = $stmt->execute($dados);

            // $result = $stmt->error;

            // return true;

            $stmt = null;

            if ($result) {

                return true;

            } else {

                //return $stmt->error;

                return false;
            }

        }
    }

    public function storeToken($email) {

        // se existir retorna com erro
        mt_srand(time());

        $token = mt_rand(1000000, 999999999) . rand() . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);

        $stmt = $this->o_db->prepare("SELECT * from token_api WHERE email = :email");

        $dados = array(":email" => $email);

        $stmt->execute($dados);

        if ($stmt->rowCount() > 0) {

            $stmt = $this->o_db->prepare("UPDATE token_api set token = :token where email = :email");

            $dados = array(":email" => $email, ":token" => $token);

            $stmt->execute($dados);

            $response = array();

            $response['email'] = $email;

            $response['token'] = $token;

            return $response;

        } else {

            $stmt = $this->o_db->prepare("INSERT INTO token_api(email,token) VALUES( :email, :token)");

            $dados = array(":email" => $email, ":token" => $token);

            $stmt->execute($dados);

            $response = array();

            $response['email'] = $email;

            $response['token'] = $token;

            return $response;

        }

    }


    /**
     * Busca todas as categorias do Fornecedor.
     * @return array|bool
     */
    public function getAllCategoria() {

        $stmt = $this->o_db->query("SELECT * FROM categoria_fornecedor");

        if ($stmt->execute()) {

            $categorias = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $categorias;

        } else {

            return false;
        }

    }


    public function checkToken($email, $token) {

        $stmt = $this->o_db->prepare("SELECT * from token_api WHERE email = :email and token = :token");

        $dados = array(":email" => $email, ":token" => $token);

        $stmt->execute($dados);

        if ($stmt->rowCount() > 0) {

            $stmt = null;

            return true;

        } else {

            $stmt = null;

            return false;
        }
    }


    public function storeParametrosPeriodo($fornecedor, $dia, $hora_inicio, $hora_fim, $status) {

        $stmt = $this->o_db->prepare("SELECT * from periodo_funcionamento WHERE email_fornecedor = :fornecedor and dia = :dia ");

        $dados = array(":fornecedor" => $fornecedor, ":dia" => $dia);

        $stmt->execute($dados);

        if ($stmt->rowCount() > 0) {

            $stmt = null;

            echo "{\"erro\": \"Parametro já cadastrado \"}";

        } else {

            $stmt = $this->o_db->prepare("INSERT INTO periodo_funcionamento (email_fornecedor,dia,hora_abertura,hora_fechamento,status) VALUES ( :fornecedor, :dia, :hora_abertura, :hora_fechamento,:status )");

            $dados = array(":fornecedor" => $fornecedor, ":dia" => $dia, ":hora_abertura" => $hora_inicio, ":hora_fechamento" => $hora_fim, ":status" => $status);

            $result = $stmt->execute($dados);

            $stmt = null;

            if ($result) {

                $stmt = $this->o_db->prepare("SELECT * from periodo_funcionamento WHERE email_fornecedor = :fornecedor and dia = :dia ");

                $dados = array(":fornecedor" => $fornecedor, ":dia" => $dia);

                $stmt->execute($dados);

                $stmt = null;

                return true;

            } else {

                echo "{\"erro\": \"$stmt->error !\"}";

                $stmt = null;
            }
        }

    }

    public function storeParametrosRegras($fornecedor, $valor, $distancia) {

        $stmt = $this->pdo->prepare("SELECT * from parametros WHERE id_fornecedor = :fornecedor  ");

        $dados = array(":fornecedor" => $fornecedor);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->rowCount() > 0) {

            $stmt = null;

            echo "{\"erro\": \"Parametro já cadastrado \"}";

        } else {

            $stmt = $this->pdo->prepare("INSERT INTO parametros (id_fornecedor,valor_minimo,distancia_entrega) VALUES ( :fornecedor, :valor, :distancia)");

            $dados = array(":fornecedor" => $fornecedor, ":valor" => $valor, ":distancia" => $distancia);

            $result = $stmt->execute($dados);

            $stmt = null;

            if ($result) {

                $stmt = $this->pdo->prepare("SELECT * from parametros WHERE id_fornecedor = :fornecedor  ");

                $dados = array(":fornecedor" => $fornecedor);

                $stmt->execute($dados);

                $stmt = null;

                return true;

            } else {

                echo "{\"erro\": \"$stmt->error !\"}";

                $stmt = null;
            }
        }

    }


    public function getFornecedoresbyDistance($latitude, $longitude) {

        $dia = jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m"), date("d"), date("Y")), 0) + 1;

        $query = "select * from (SELECT id_fornecedor,nome,frase, email,bairro,rua,cep,complemento,cidade,estado,latitude,longitude,c.descricao as categoria,img,status,distancia_entrega,( 6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( latitude ) ) ) ) AS distance FROM fornecedores f, categoria_fornecedor c where f.categoria=c.id_categoria_fornecedor ) as fornecedores, (select * from periodo_funcionamento pf where  dia=" . $dia . " and status='A' ) as func where func.email_fornecedor=fornecedores.email and distance < distancia_entrega and fornecedores.status='A'  ORDER BY rand()";

        $stmt = $this->o_db->query($query);

        $dados = array(":latitude" => $latitude, ":longitude" => $longitude);

        if ($stmt->execute($dados)) {

            $fornecedores = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            $i = 0;

            if ( ! empty($fornecedores) ) {

                foreach ($fornecedores as $value) {

                    $consulta = $this->o_db->query("select * from forma_pagamento a, pagamento_fornecedor b where email_fornecedor='" . $value['email'] . "' and a.id_pagamento=b.id_pagamento");

                    $consulta->execute();

                    $result2 = $consulta->fetchAll(PDO::FETCH_ASSOC);

                    $res[$i] = array("fornecedor" => $value, "pagamento" => $result2);

                    $i++;
                }

            return $res;

            } else {

                return 'Nenhum Fornecedor encontrado';
            }

        } else {

            return false;
        }
    }


    public function storeParametrosPagamento($fornecedor, $id_forma_pagamento) {

        $stmt = $this->o_db->prepare("SELECT * from pagamento_fornecedor WHERE email_fornecedor = :fornecedor and id_pagamento = :id_pagamento ");

        $dados = array(":fornecedor" => $fornecedor, ":id_pagamento" => $id_forma_pagamento);

        $stmt->execute($dados);

        if ($stmt->rowCount() > 0) {

            $stmt = null;

            echo "{\"erro\": \"Parametro já cadastrado \"}";

        } else {

            $stmt = $this->o_db->prepare("INSERT INTO pagamento_fornecedor (email_fornecedor,id_pagamento) VALUES ( :email_fornecedor, :id_pagamento)");

            $dados = array(":email_fornecedor" => $fornecedor, ":id_pagamento" => $id_forma_pagamento);

            $result = $stmt->execute($dados);

            $stmt = null;

            if ($result) {

                return true;

            } else {

                echo "{\"erro\": \"$stmt->error !\"}";

                $stmt = null;
            }
        }

    }

    function getFornecedor($latitude, $longitude, $id) {

        $dia = jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m"), date("d"), date("Y")), 0) + 1;

        $consulta = $this->o_db->query("select * from (SELECT id_fornecedor,nome,frase, email,bairro,rua,cep,complemento,cidade,estado,valor_frete,valor_minimo,latitude,longitude,c.descricao as categoria,img,status,distancia_entrega,( 6371 * acos( cos( radians(".$latitude.") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$longitude.") ) + sin( radians(".$latitude.") ) * sin( radians( latitude ) ) ) ) AS distance FROM fornecedores f, categoria_fornecedor c where f.categoria=c.id_categoria_fornecedor ) as fornecedores, (select * from periodo_funcionamento pf where  dia=" . $dia . " and status='A' ) as func where func.email_fornecedor=fornecedores.email and distance < distancia_entrega and fornecedores.status='A' and fornecedores.id_fornecedor=".$id."  ORDER BY rand()");

        $consulta->execute();

        $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $i = 0;

        $res = array();

        foreach ($result as $value) {

            $consulta = $this->o_db->query("select * from forma_pagamento a, pagamento_fornecedor b where email_fornecedor='" . $value['email'] . "' and a.id_pagamento=b.id_pagamento ");

            $consulta->execute();

            $result2 = $consulta->fetchAll(PDO::FETCH_ASSOC);

            $res[$i] = array("fornecedor" => $value, "pagamento" => $result2);

            $i++;
        }

        return $res;

    }


    function getCardapioFornecedor($id) {

        $query = "select id_cardapio,c.nome as nome, c.descricao as descricao, tempo_preparo,valor, categoria,fornecedor, id_categoria,img from cardapio c , categoria g  where  c.categoria=g.id_categoria and c.fornecedor = ".$id." and c.status='A'";

        $consulta =  $this->o_db->query($query);

        $consulta->execute();

        $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getCardapiobyId($id_cardapio, $id_fornecedor) {

        $consulta =  $this->o_db->query("select id_cardapio,c.nome as nome, c.descricao as descricao, tempo_preparo,valor, categoria,fornecedor, id_categoria,img from cardapio c , categoria g  where  c.categoria=g.id_categoria and c.fornecedor = ".$id_fornecedor." and c.id_cardapio = ".$id_cardapio." and c.status='A'");

        $dados = array(":id_cardapio" => $id_cardapio, ":id_fornecedor" => $id_fornecedor);

        $consulta->execute($dados);

        $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

        $ress = array();

        $i = 0;

        foreach ($result as $value) {

            $consulta =  $this->o_db->query("select * from complemento where id_fornecedor = ".$id_fornecedor." and status = 'A' and categoria = " . $value['categoria']);

            $dados = array(":id_fornecedor" => $id_fornecedor);

            $consulta->execute($dados);

            $result2 = $consulta->fetchAll(PDO::FETCH_ASSOC);

            $res[$i] = array("cardapio" => $value, "complemento" => $result2);

            $i++;
        }

        return $res;
    }

    /**
     * @return mixed
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome) {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getFrase()  {
        return $this->frase;
    }

    /**
     * @param mixed $frase
     */
    public function setFrase($frase)  {
        $this->frase = $frase;
    }

    /**
     * @return mixed
     */
    public function getSenha() {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha) {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBairro() {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getRua() {
        return $this->rua;
    }

    /**
     * @param mixed $rua
     */
    public function setRua($rua) {
        $this->rua = $rua;
    }

    /**
     * @return mixed
     */
    public function getCep() {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep) {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getComplemento() {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento) {
        $this->complemento = $complemento;
    }

    /**
     * @return mixed
     */
    public function getCidade() {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    /**
     * @return mixed
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado) {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt) {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getDataCadastro() {
        return $this->data_cadastro;
    }

    /**
     * @param mixed $data_cadastro
     */
    public function setDataCadastro($data_cadastro) {
        $this->data_cadastro = $data_cadastro;
    }

    /**
     * @return mixed
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCgc() {
        return $this->cgc;
    }

    /**
     * @param mixed $cgc
     */
    public function setCgc($cgc) {
        $this->cgc = $cgc;
    }

    /**
     * @return mixed
     */
    public function getCategoria() {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getDistanciaEntrega() {
        return $this->distancia_entrega;
    }

    /**
     * @param mixed $distancia_entrega
     */
    public function setDistanciaEntrega($distancia_entrega) {
        $this->distancia_entrega = $distancia_entrega;
    }

    /**
     * @return mixed
     */
    public function getValorMinimo() {
        return $this->valor_minimo;
    }

    /**
     * @param mixed $valor_minimo
     */
    public function setValorMinimo($valor_minimo) {
        $this->valor_minimo = $valor_minimo;
    }

    /**
     * @return mixed
     */
    public function getValorFrete() {
        return $this->valor_frete;
    }

    /**
     * @param mixed $valor_frete
     */
    public function setValorFrete($valor_frete) {
        $this->valor_frete = $valor_frete;
    }

    /**
     * @return mixed
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero) {
        $this->numero = $numero;
    }

    /**
     * @return resource
     */
    public function getODb() {
        return $this->o_db;
    }

    /**
     * @param resource $o_db
     */
    public function setODb($o_db) {
        $this->o_db = $o_db;
    }


}